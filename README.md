# About
Emdl is an extensible manga (comic/webtoon/anime/gallery) downloader (reader/viewer) using Electron because slapping Chromium at something makes it better by default.
Aside from this it enables easy adding and editing of plugins without having to rebuild binaries from the source.
By using JavaScript the workflow from drafting in the web console to the creation of a working plugin is simplified.


You can get the binaries (win, mac, linux) [here](https://copynippyjoin.gitlab.io/emdl-website/).


The latest developer build (can be buggy/unstable) is available [here](https://gitlab.com/CopyNippyJoin/emdl/-/jobs/artifacts/master/download?job=package). 

Screenshots: [reader](https://copynippyjoin.gitlab.io/emdl-website/screenshots/screenshot1.jpg), 
[reader settings](https://copynippyjoin.gitlab.io/emdl-website/screenshots/screenshot2.jpg), 
[metadata](https://copynippyjoin.gitlab.io/emdl-website/screenshots/screenshot3.jpg), 
[downloader](https://copynippyjoin.gitlab.io/emdl-website/screenshots/screenshot4.jpg)

# Versioning
For releases [semantic versioning](https://semver.org/) is used.

# Building
To build the application you need [node.js](https://nodejs.org/en/).
1. Clone the repository.
2. Change into its root directory and run `npm install` to install all dependencies.
3. Run `npm run start` to start the application or `npm run pack` to build the application.

# Plugin API
The plugins are located under `js/mmodels`, if you downloaded the binaries it may vary a bit (e.g. for win it should be `resources/app/src/js/mmodels`).
To add your own ones just paste them in this folder and they will be loaded. 
Most websites are usually structured in a similar manner, thus new plugins can be implemented by simple copy & paste and adjusting some query selectors/regex.

For further documentation you can check out:
  - [The code of the existing plugins](https://gitlab.com/CopyNippyJoin/emdl/tree/master/js/mmodels)
  - [The specifications](https://gitlab.com/CopyNippyJoin/emdl/tree/master/docs/Specifications.md), which simplifies the creation of plugins by offering shared functionality.

# License
GPLv3
