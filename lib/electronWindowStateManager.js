'use strict';

const fs = require('fs');
const path = require('path');
const electron = require('electron');

const app = electron.app || electron.remote.app;
const screen = electron.screen || electron.remote.screen;

const debounce = (fn, wait = 500) =>
{
  let timeout
  return function (...args) {
    clearTimeout(timeout)
    timeout = setTimeout(() => fn.call(this, ...args), wait)
  }
}

class ElectronWindowState
{
  constructor(options)
  {
    this.setOptions(options);
    this.loadState();
  }

  setOptions(options)
  {
    options.defaultState = Object.assign({ x:0, y:0, height: 800, width: 600, maximized: false, fullscreen: false }, options.defaultState);
    this.options = Object.assign({
      fileName: 'electronWindowState.json',
      path: app.getPath('userData'),
      debounceTime: 500,
      checkVisibility: true
    }, options);
    this.absoluteFilePath = path.join(this.options.path, this.options.fileName);
    
    let electronWindow = this.electronWindow;
    if(electronWindow)
      this.unbindWindow();

    this.debouncedSaveState = debounce(this.saveState, this.options.debounceTime);
    this.boundSaveState          = () => this.saveState();
    this.boundDebouncedSaveState = () => this.debouncedSaveState();

    if(electronWindow)
      this.bindWindow(electronWindow);
  }

  bindWindow(electronWindow)
  {
    if(this.electronWindow)
      this.unbindWindow();
    this.electronWindow = electronWindow;

    this.electronWindow.on('resize', this.boundDebouncedSaveState);
    this.electronWindow.on('move'  , this.boundDebouncedSaveState);
    this.electronWindow.on('close' , this.boundSaveState         );

    if(this.state.maximized)
      this.electronWindow.maximize();
    if(this.state.fullscreen)
      this.electronWindow.setFullScreen(true);
  }
  unbindWindow()
  {
    this.electronWindow.off('resize', this.boundDebouncedSaveState);
    this.electronWindow.off('move'  , this.boundDebouncedSaveState);
    this.electronWindow.off('close' , this.boundSaveState         );
    this.electronWindow = undefined;
  }

  stateIsVisibile(state)
  {
    return screen.getAllDisplays().some(display => {
      let isVisible = state.x + state.width - 150 >= display.bounds.x  &&
                      state.y + 10  >= display.bounds.y &&
                      state.x + 150 <= display.bounds.x + display.bounds.width &&
                      state.y + 150 <= display.bounds.y + display.bounds.height;
      return isVisible;
    });    
  }
  loadState()
  {
    try
    {
      if(fs.existsSync(this.absoluteFilePath))
      {
        this.state = JSON.parse(fs.readFileSync(this.absoluteFilePath));
        if(this.options.checkVisibility && !this.stateIsVisibile(this.state))
          this.state = this.options.defaultState;
      }  
      else
        this.state = this.options.defaultState;
    }
    catch(e)
    {
      this.state = this.options.defaultState;
      console.error(e)
    }
  }
  saveState()
  {
    if(!this.electronWindow.isMinimized())
    {
      this.state.maximized  = this.electronWindow.isMaximized();
      this.state.fullscreen = this.electronWindow.isFullScreen();
      if(!this.state.maximized && !this.state.fullscreen)
        this.state = Object.assign(this.state, this.electronWindow.getBounds());    
    }

    if(!fs.existsSync(this.options.path))
      fs.mkdirSync(this.options.path);
    fs.writeFileSync(this.absoluteFilePath, JSON.stringify(this.state));
  }
}

module.exports = ElectronWindowState;