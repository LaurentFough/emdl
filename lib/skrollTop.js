(function (root, factory) {
  if ( typeof define === 'function' && define.amd ) {
    define([], (function () {
      return factory(root, root.document);
    }));
  } else if ( typeof exports === 'object' ) {
    module.exports = factory(root, root.document);
  } else {
    root.skrollTop = factory(root, root.document);
  }
})(typeof global !== 'undefined' ? global : typeof window !== 'undefined' ? window : this, (function (window, document) {
    'use strict';

    Math.easeInOutCubic = (t, b, c, d) => {
      if((t/=d/2) < 1)
        return c/2*t*t*t + b;
      return c/2*((t-=2)*t*t + 2) + b;
    };

    let self = {};

    let stopAnimation;
    self.stop = () => stopAnimation = true;

    self.scrollTo = (params) =>
    {
      let scrollDirection = params.scrollDirection ? params.scrollDirection : "scrollTop";
      let element         = params.element         ? params.element         : window;
      let duration        = params.duration        ? params.duration        : 700;
      let callback        = params.callback        ? params.callback        : null;
      let easing          = params.easing          ? params.easing          : Math.easeInOutCubic;
      let to              = params.to              ? params.to              : start + 15;

      let start;
      if(scrollDirection == "scrollTop")
        start = element!==window ? element.scrollTop : (window.pageYOffset || document.documentElement.scrollTop)  - (document.documentElement.clientTop || 0);
      if(scrollDirection == "scrollLeft")
        start = element!==window ? element.scrollLeft : (window.pageXOffset || document.documentElement.scrollLeft)  - (document.documentElement.clientLeft || 0);

      let change = to - start;
      let currentTime = 0;
      let increment;
      let lastTime;

      stopAnimation = false;
      
      let animateScroll = (curTime) =>
      {
          if(!lastTime) 
              lastTime = curTime;
          increment = curTime - lastTime;
          lastTime = curTime;
          if(stopAnimation)
          {
            if(currentTime < duration)
              callback && callback();
          }
          else
          {
            currentTime += increment;
            element[scrollDirection] = easing(currentTime, start, change, duration);
            if(currentTime < duration)
              requestAnimationFrame(animateScroll);
            else
              callback && callback();
          }
      };

      requestAnimationFrame(animateScroll);
   };

   return self;
}));
