"use strict";

window.fs = require('fs-extra');
window.nodeUtil = require('./util/nodeUtil.js');

angular.module('app', [
	'ngMaterial', 'ui.bootstrap.contextMenu',
	'downloader', 'reader', 'mediaViewer',
	'metaData', 'updateCheck', 
]);
angular.module('app').config(
["$mdThemingProvider", "$provide", "$compileProvider", "$mdAriaProvider", "$mdGestureProvider",
( $mdThemingProvider ,  $provide ,  $compileProvider ,  $mdAriaProvider, $mdGestureProvider) =>
{
	fs.ensureDirSync(userPaths.themes);
	let files = fs.readdirSync(userPaths.themes);
	for(let file of files)
	{
		let themeString = fs.readFileSync(path.join(userPaths.themes, file), {encoding: "utf8"}).replace(/'/g, '"');
		userThemes[file] = JSON.parse(themeString);
		$mdThemingProvider.definePalette(nodeUtil.removeFileExtension(file), JSON.parse(themeString));
	}
	$mdThemingProvider.generateThemesOnDemand(true);
	$provide.value('themeProvider', $mdThemingProvider);
	
	$mdAriaProvider.disableWarnings();
	$mdGestureProvider.disableAll();
	$compileProvider.debugInfoEnabled(isPackaged);
	$compileProvider.commentDirectivesEnabled(false);
	$compileProvider.cssClassDirectivesEnabled(false);
}]);

angular.module('app').controller('appCtrl',
["$rootScope", "$scope", "ngUtil",
( $rootScope ,  $scope ,  ngUtil ) =>
{
	debugLog($scope);

	let defaultConfig =
	{
		config:
		{
			search:"",
			dlreppath:"%MediaTitle%/v%Volume%/c%Chapter%/%FileName%.%DataType%",
			format:"raw",
			dldir:userPaths.downloads,
			mmodeloptions:{},
			fetchRemoteScript:false,
			enableBackgroundThrottling:false,
			colorTheme:
			{
				primaryPalette:'light-blue',
				accentPalette:'red',
				warnPalette:'yellow',
				backgroundPalette:'grey',
				darkTheme:true
			},
			loaderCustomFiles: []
		},
		reader:
		{
			config:
			{
				show:false,
				zoomFac:1,
				dragscroll:false,
				dragScrollFactor:1,
				enableFileHistory:true,
				enableEndlessScroll:false,
				enableSmoothScroll:true,
				hideSideMenu:false,
				videoAutoPlay:false,
				videoLoop:false,
				numericSorting:true,
				basedir:userPaths.downloads,
				styleContainer:{"overflow": "hidden", "background-color":"rgb(0,0,0)"},
				styleImage:{"max-width": "100%"},
				pinnedDirs:[],
				keyConfig:
				[
					{type:"ArrowLeft",  fct:"changeFile",       x:-1  },
					{type:"ArrowRight", fct:"changeFile",       x: 1  },
					{type:"ArrowDown",  fct:"scrollVertical",   x: 15 },
					{type:"ArrowUp",    fct:"scrollVertical",   x:-15 },
					{type:"-",          fct:"changeZoom",       x:-0.1},
					{type:"+",          fct:"changeZoom",       x: 0.1},
					{type:"F5",         fct:"reloadApp"               },
					{type:"F9",         fct:"toggleReader"            },
					{type:"F10",        fct:"toggleSideMenu"          },
					{type:"F11",        fct:"toggleFullscreen"        },
					{type:"F12",        fct:"toggleDevTools"          },
					{type:"PageUp",     fct:"changeFile",       x:-25 },
					{type:"PageDown",   fct:"changeFile",       x: 25 },
					{type:"Numpad1",    fct:"scrollHorizontal", x:-15 },
					{type:"Numpad2",    fct:"scrollHorizontal", x: 15 },
					{type:"Delete",     fct:"deleteCurrentFile"       }
				],
				mouseConfig:
				[
					{type:"LeftClick",  fct:"changeFile",       x: 1  },
					{type:"WheelClick", fct:"toggleSideMenu"          },
					{type:"RightClick", fct:"changeFile",       x:-1  },
					{type:"Backward",   fct:"changeFile",       x:-1  },
					{type:"Forward",    fct:"changeFile",       x: 1  },
					{type:"WheelUp",    fct:"scrollVertical",   x:-15 },
					{type:"WheelDown",  fct:"scrollVertical",   x: 15 },
				]
			},
			readerFileHistory:{},
			readerLastOpenDir:""
		}
	};
	
	if(fs.existsSync(userPaths.preConfigFile))
		try{$rootScope.preConfig = fs.readJSONSync(userPaths.preConfigFile) } catch(e) { console.log("failed loading preConfigFile"); }
	$rootScope.preConfig = $rootScope.preConfig || {singleInstanceLock:true, configStorageType:"localStorage"};

	if($rootScope.preConfig.configStorageType == "file" && fs.existsSync(userPaths.configFile))
		try{$rootScope.$storage = fs.readJSONSync(userPaths.configFile) } catch(e) { console.log("failed loading configFile"); }
	if($rootScope.preConfig.configStorageType != "file")
		$rootScope.$storage = JSON.parse(localStorage.getItem("config"));
	$rootScope.$storage = $rootScope.$storage || defaultConfig;
	ngUtil.addMissingProps(defaultConfig, $rootScope.$storage, ["styleContainer","styleImage"]);

	let handlePreConfigChange = async () =>
	{
		if(!$rootScope.preConfig.singleInstanceLock)
		{
			$rootScope.preConfig.configStorageType = "file";
			remote.app.releaseSingleInstanceLock();
		}
		else
			remote.app.requestSingleInstanceLock();
	}
	handlePreConfigChange();
	
	let storePreConfig = () =>
	{
		handlePreConfigChange();
		fs.writeJSONSync(userPaths.preConfigFile, $rootScope.preConfig);
	}	
	let storeConfig = () =>
	{
		if($rootScope.preConfig.configStorageType == "file")
			fs.writeJSONSync(userPaths.configFile, $rootScope.$storage);
		else
			localStorage.setItem("config", JSON.stringify($rootScope.$storage));
	}
	$rootScope.storeAllConfig = () => 
	{
		storePreConfig();
		storeConfig();
	}
	remote.globalShortcut.register('Alt+Space', () => {});
	window.addEventListener('beforeunload', () =>
	{
		remote.globalShortcut.unregister('Alt+Space');
		$rootScope.storeAllConfig();
	});
	setInterval(storeConfig, 300000);

	for(let filePath of $rootScope.$storage.config.loaderCustomFiles)
	{
		try{
			if(path.extname(filePath) == ".js")
				nodeRequire(filePath);
			if(path.extname(filePath) == ".css")
				loadCSS(filePath);
		}
		catch(e){console.error(e);}
	}
	ngUtil.updateColorTheme();
	ngUtil.setBackgroundThrottling();
}]);
