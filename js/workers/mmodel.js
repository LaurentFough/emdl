"use strict"; 

const nodeRequire = self.require;
const mmSharedNode = require("../util/mmSharedNode.js");
const nodeUtil = require("../util/nodeUtil.js");

let mmodel;
let options;

onmessage = async function(ev)
{
	try
	{
		if(ev.data.method == "setOptions")
		{
			mmodel = nodeRequire(ev.data.paths.mmodel);
			options = ev.data.options;
			if(options.debug)
				mmSharedNode.debug = true;
		}
		else
			ev.data.options = options;

		let response;
		if(ev.data.method == "pextract")
		{
			response = await mmodel.pextract(mmSharedNode, ev);
			if(response)
				response = mmSharedNode.normalizePageNumbers(response);
		}
		if(ev.data.method == "iextract")
			response = await mmodel.iextract(mmSharedNode, ev);
		postMessage({ "data" : response});
	}
	catch(e)
	{
		postMessage({ "error" : e.stack	});
	}
};