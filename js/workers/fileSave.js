"use strict"; 

const fs = require('fs-extra');
const mime = require('mime-types');
const requestWrapper = require('../../js/util/requestWrapper.js');
const nodeUtil = require("../../js/util/nodeUtil.js");


let removeWatermarks = async (body, mmodelPath, targetPath) =>
{
	const Jimp = require("jimp");
	let image = await Jimp.read(body);
	let mmodel = self.require(mmodelPath);
	
	let watermarkConfig = mmodel.getWatermarkConfig(image, nodeUtil);
	for(let watermarkFunctions of watermarkConfig)
	{
		try
		{
			if(watermarkFunctions.matchFunction(image))
				watermarkFunctions.removeFunction(image);
		}
		catch(e)
		{
			console.log("debug", e);
		}
	}
	await image.writeAsync(targetPath);
}


let saveFile = async (eventData) =>
{
	let attempt = 0;
	let maxAttempts = 3;
	for(; attempt<maxAttempts; attempt++)
	{
		try
		{
			let response = await requestWrapper.getRequest(eventData.page.options, eventData.page.options.requestCfg);
			let datatype = response.headers ? mime.extension(response.headers["content-type"].split(";")[0]) : null;
			
			if(datatype == "jpeg" || !datatype)
				datatype = "jpg";
	
			if(datatype != "html")
			{
				eventData.page.targetPath = eventData.page.targetPath.replace(/\%DataType\%/g, datatype);
				if(eventData.mmodelPath)
					await removeWatermarks(response.data, eventData.mmodelPath, eventData.page.targetPath);
				else
				{
					let writeStream = fs.createWriteStream(eventData.page.targetPath);
						response.data.pipe(writeStream);
					await new Promise((resolve, reject) => {
						writeStream.on('finish', resolve)
						writeStream.on('error', reject)
					});
					if(response.headers && response.headers["content-length"] && writeStream.bytesWritten != response.headers["content-length"])
						throw new Error('content-length mismatch');
				}
				break;
			}
		}
		catch(e)
		{
			console.log("debug", { desc: "save-worker: "+attempt+". failed attempts to receive a file from " + eventData.page.src, input: eventData, error:e.stack });
		}
	}

	if(attempt >= maxAttempts)
		postMessage({ "error": { desc: "save-worker: "+maxAttempts+" failed attempts to receive a file from " + eventData.page.src, input: eventData } });
	else
		postMessage({ "success": true, "input": eventData });	
}

onmessage = async (ev) =>
{
	let eventData = ev.data;
	if(!eventData.page.options)
		eventData.page.options = {};
	let dirpath = eventData.page.targetPath.replace(/\\/g, "");
	dirpath = eventData.page.targetPath.substring(0, dirpath.lastIndexOf("/") + 1);
	await fs.ensureDir(dirpath);

	if(eventData.page && eventData.page.src)
	{
		let requestCfg = {url : eventData.page.src, headers: {"User-Agent": "Mozilla/5.0"}};
		

		requestCfg.responseType = "stream";
		if(eventData.mmodelPath)
			requestCfg.responseType = "arraybuffer";

		eventData.page.options.requestCfg = Object.assign(requestCfg, eventData.page.options.requestCfg || {});
		saveFile(eventData);
	}
	else
		postMessage({ "error": { desc: "save-worker: received empty file source", input: eventData} });
};