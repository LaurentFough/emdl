"use strict"; 

const fs = require('fs-extra');

let packCbz = function(ev)
{
	try
	{
		let zipdir = require('zip-dir');
		let dir = ev.data.entryFolder.replace(/\\/g,"/");
		zipdir(dir, { saveTo: dir + '.cbz' }, () => postMessage("generating cbz finished"));	
	}
	catch(e)
	{
		console.log("debug", e);
	}
}
let packPdf = async function(ev)
{
	try
	{
		let getImageSize = require('image-size');
		let PDFDocument = require('pdfkit/js/pdfkit.standalone.js');
		let nodeUtil = require("../util/nodeUtil.js");
		let dir = ev.data.entryFolder.replace(/\\/g,"/");
		let imageFiles = await nodeUtil.getFilesRecursive(dir);
		imageFiles = imageFiles.sort(nodeUtil.sortFilesHelper);

		let doc = new PDFDocument({"autoFirstPage": false});
		for(let i=0; i<imageFiles.length; i++)
		{
			try
			{
				let imageSize = getImageSize(imageFiles[i]);
				doc.addPage({size:[imageSize.width, imageSize.height]});
				let base64Data = await fs.readFile(imageFiles[i], { encoding: 'base64' });
				doc.image("data:image;base64," + base64Data, 0, 0, imageSize);
			}
			catch(e){}
		}

		let writeStream = fs.createWriteStream(ev.data.entryFolder + ".pdf");
		writeStream.on('finish', () => postMessage("generating pdf finished"));
		doc.end();
		doc.pipe(writeStream);
	}
	catch(e)
	{
		console.log("debug", e);
	}
}
let packEpub = async function(ev)
{
	try
	{
		let os = require('os');
		let Epub = require("epub-gen");

		let option = {
			"title": ev.data.media.name,
			"author": "emdl",
			"tempDir": os.tmpdir()
		};

		try
		{
			option.css = require("!!to-string-loader!css-loader!epub-gen/templates/template.css");
			option.customOpfTemplatePath  = require("!!file-loader!epub-gen/templates/epub3/content.opf.ejs").default;
			option.customHtmlTocTemplatePath = require("!!file-loader!epub-gen/templates/epub3/toc.xhtml.ejs").default;
			option.customNcxTocTemplatePath = require("!!file-loader!epub-gen/templates/toc.ncx.ejs").default;
		}
		catch(e)
		{
			console.log(e);
		}
		// workaround for epub gen bug
		if(ev.data.entryFolder.includes("'"))
		{
			for(let page of ev.data.media.completedPages)
			{
				await fs.move(page.targetPath, page.targetPath.replace(/'/g, ""));
				page.targetPath = page.targetPath.replace(/'/g, "");
			}
			await fs.remove(ev.data.entryFolder);
		}

		let contentData = [];
		let lastVolnumb;
		let lastChanumb;
		for(let page of ev.data.media.completedPages)
		{
			if(lastVolnumb != page.volnumb || lastChanumb != page.chanumb)
			{
				lastVolnumb = page.volnumb;
				lastChanumb = page.chanumb;
				contentData.push({ title: "Volume " + page.volnumb + " - Chapter " + page.chanumb, data: ""});
			}
			contentData[contentData.length - 1].data += "<img src='"+page.targetPath+"'></img>";
            console.log("page.targetPath", page.targetPath)
		}

		
		option.content = contentData;
		await (new Epub(option, ev.data.entryFolder+".epub")).promise;
		postMessage("generating epub finished");
	}
	catch(e)
	{
		console.log("debug", e);
	}
}

onmessage = function (ev)
{
	if(ev.data.format=="cbz")
		packCbz(ev);
	if(ev.data.format=="pdf")
		packPdf(ev);
	if(ev.data.format=="epub")
		packEpub(ev);
};