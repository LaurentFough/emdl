"use strict";

if(isPackaged)
{
    require("node_modules/typeface-roboto/index.css");
    require("node_modules/material-icons/iconfont/material-icons.css");
    require("node_modules/angular-material/angular-material.min.css");
    require("node_modules/plyr/dist/plyr.css");
    require("css/plyr.css");
    require("css/app.css");
}
else
{
    loadCSS("node_modules/typeface-roboto/index.css");
    loadCSS("node_modules/material-icons/iconfont/material-icons.css");
    loadCSS("node_modules/angular-material/angular-material.min.css");
    loadCSS("node_modules/plyr/dist/plyr.css");
    loadCSS("css/plyr.css");
    loadCSS("css/app.css");
}

	
window.ace = require("ace-builds/src-min-noconflict/ace");
require("ace-builds/src-min-noconflict/theme-twilight.js");
require("ace-builds/src-min-noconflict/mode-json.js");
ace.require('ace/tooltip').Tooltip.prototype.setPosition = function (x, y){
    let boundingRect = this.$parentNode.getBoundingClientRect();
    x -= boundingRect.left;
    y -= boundingRect.top;
    this.getElement().style.left = x + "px";
    this.getElement().style.top = y + "px";
};
if(isPackaged)
{
    require("!!file-loader?name=worker-json.js!ace-builds/src-min-noconflict/worker-json.js");
    ace.config.set('workerPath', "../app.asar.unpacked/build");
}
else
    ace.config.set('workerPath','node_modules/ace-builds/src-min-noconflict');

window.skrollTop = require("lib/skrollTop.js");
window.Plyr = require("plyr");
window.angular = require("angular");
require("angular-animate");
require("angular-material");

require("angular-bootstrap-contextmenu");

require("lib/ng-drag-scroll.js");
