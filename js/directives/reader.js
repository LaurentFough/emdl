const yauzl = require( 'yauzl-promise');
const klaw = require('klaw');
const through2 = require('through2');


angular.module("reader", ['ngMaterial', 'ng-drag-scroll']).directive("readerUi",
["$rootScope", "$mdDialog", "$mdToast", "ngUtil", "mediaViewerFactory",
( $rootScope ,  $mdDialog ,  $mdToast ,  ngUtil ,  mediaViewerFactory) =>
{
    return {
		restrict : 'E',
        scope: {},
        templateUrl : 'js/directives/reader.html',
		link: function(scope, element, attrs, controllers)
		{
			// TODO: endless scroll performance; img decoding; pdf open; video controls zoom/scroll;
			// TODO: key functions multipile params; config max deepth; download MetaData & options to show folder as gallery
			// TODO: directive for dirs
			debugLog(scope);
		
			//init
			let readerBaseDir = document.getElementById("readerBaseDir");
			scope.ngUtil = ngUtil;
			scope.storage = $rootScope.$storage.reader;
			scope.config = scope.storage.config;
			scope.rootdirs = [];

			
			scope.menuFolders = 
			[
				['Open', function ($itemScope, $event, modelValue, text, $li) {
					scope.config.basedir = (modelValue || $itemScope.dir).path.replace(/\\/g, "/");
				}],
				['Open External', function ($itemScope, $event, modelValue, text, $li) {
					electron.shell.openExternal((modelValue || $itemScope.dir).path.replace(/\\/g, "/"));
				}],
				['Toggle Pin', function ($itemScope, $event, modelValue, text, $li) {
					let dir = (modelValue || $itemScope.dir);
					if(!dir.pinned)
						scope.config.pinnedDirs.push(dir);
					else
						scope.config.pinnedDirs = scope.config.pinnedDirs.filter( obj => obj.path != dir.path);
					dir.pinned = !dir.pinned;
					loadBaseDir();
				}],
				['Read', function ($itemScope, $event, modelValue, text, $li) {
					scope.openm.loadPath((modelValue || $itemScope.dir).path);
				}],
				['Rename', async function ($itemScope, $event, modelValue, text, $li) {
					let dir = (modelValue || $itemScope.dir);
					dir.path = await renameFileModal(dir.path);
					dir.name = dir.path.split("/").filter(Boolean).slice(-1)[0];
				}],
				['Delete', async function ($itemScope, $event, modelValue, text, $li) {
					await deletePathModal((modelValue || $itemScope.dir).path);
				}],
				['Metadata', function ($itemScope, $event, modelValue, text, $li) {
					ngUtil.showMetaInfo((modelValue || $itemScope.dir).name,$event);
				}]
			];

			//entry glue
			let entryWatcher = scope.$watch( () => mediaViewerFactory.openm, (newVal, oldVal) => 
			{
				if(!mediaViewerFactory.openm)
					return;
				scope.openm = new ReaderFolder(mediaViewerFactory.openm);

				if(processArgs(remote.process.argv))
					remote.process.argv = remote.process.argv.slice(0,-1);
				else
				{
					if(scope.config.show && scope.config.enableFileHistory && scope.storage.readerLastOpenDir)
						scope.openm.loadPath(scope.storage.readerLastOpenDir);
				}
	
				$rootScope.$watch("$storage.reader.config.show", () => $rootScope.$storage.reader.config.show ? scope.$resume() : scope.$suspend());
				entryWatcher();
			});


			//glue
			let MediaFile = mediaViewerFactory.MediaFile;
			mediaViewerFactory.config = scope.config;
			mediaViewerFactory.overlay.left.push(`<i class="material-icons reader-ui-icons" ng-click="config.hideSideMenu = !config.hideSideMenu;">
				{{config.hideSideMenu?'&#xE5CC;':'&#xE5CB;'}}
			</i>`);
			
			ngUtil.loadPreview = (files) =>
			{
				$rootScope.$apply( () =>
				{
					scope.openm.depth = 0;
					scope.openm.mediaFolder.files = files.map( file => new MediaFile(file.src) );
					scope.config.show = true;
				});				
				scope.openm.mediaFolder.setFilesLength();
				scope.openm.mediaFolder.setFileIndex(0);
			}
			mediaViewerFactory.onFileChange = () => 
			{
				scope.openm.setTitle();
				if(!scope.openm.mediaFolder.loading && scope.config.enableFileHistory && scope.openm.depth!==0)
					scope.storage.readerFileHistory[scope.openm.mediaFolder.path] = scope.openm.mediaFolder.fileIndex;
			}

			//load folder
			let lastWalkerStream;
			let lastWalkerId = 0;
			class ReaderFolder
			{
				constructor(mediaFolder)
				{
					this.mediaFolder = mediaFolder;
					this.fileReference;
					this.depth;
				}
				reset()
				{
					this.mediaFolder.reset();
					this.fileReference = undefined;
					this.depth = undefined;
				}
				loadPath(path)
				{
					this.reset(); 
					this.mediaFolder.path = path;
					this.openNewFolder();
				}

				setTitle()
				{
					let pathTitle = "emdl | ";
					let currentFile = this.mediaFolder.files[this.mediaFolder.fileIndex];
					if(currentFile)
					{
						pathTitle += currentFile.path;
						if(currentFile.zipPath)
							pathTitle += currentFile.zipPath;
						pathTitle += " | " + (this.mediaFolder.fileIndex+1) + "/" + this.mediaFolder.files.length;
					}
					else
						pathTitle += this.mediaFolder.path;
					document.title = pathTitle;
				}

				async openNewFolder()
				{
					this.mediaFolder.path = this.mediaFolder.path.replace(/\\/g, "/");
					let fileCheck = new MediaFile(this.mediaFolder.path);
					if( fileCheck.mimeType && fileCheck.mediaType!="zip" )
					{
						this.fileReference = this.mediaFolder.path;
						this.mediaFolder.path = this.mediaFolder.path.substring(0, this.mediaFolder.path.lastIndexOf("/"));
						setTimeout( () => this.mediaFolder.setMediaDisplaySrc(new MediaFile(this.fileReference.replace(/\\/g,"/"))), 0);
						this.depth = 0;
					}

					this.mediaFolder.loading = true;
					this.setTitle();
	
					if(fs.existsSync(this.mediaFolder.path))
						await this.loadFolder();
					this.filesLoadFinished();
				}

				pushFile(fileData)
				{
					if(fileData.mediaType!="image" && fileData.mediaType!="video" && fileData.mediaType!="pdf")
						return;
					this.mediaFolder.files.push(fileData);
					if(this.mediaFolder.files.length==1 && !this.fileReference)
						this.mediaFolder.setFileIndex(0);
					this.mediaFolder.setFilesLength();
				}

				loadFolder()
				{	
					let self = this;
					return new Promise( (resolve, reject) =>
					{
						lastWalkerStream && lastWalkerStream.destroy();
						lastWalkerId++;
						let walkerId = lastWalkerId;
						let zipPromises =  [];
	
						let excludeDirFilter = through2.obj(function (item, enc, next)
						{
							if(!item.stats.isDirectory())
								this.push(item);
							next();
						});
						lastWalkerStream = klaw(self.mediaFolder.path, {depthLimit: self.depth})
						lastWalkerStream.pipe(excludeDirFilter)
						.on('data', item => 
						{
							let mediaFile = new MediaFile(item.path.replace(/\\/g,"/"));
							if(mediaFile.mediaType=="zip" && self.depth!==0)
								zipPromises.push(self.loadZip(mediaFile.path, walkerId));
							self.pushFile(mediaFile);
						})
						.on('end',  async () => 
						{
							await Promise.all(zipPromises);
							if(lastWalkerId == walkerId)
								resolve(); 
						});
					});
				}

				async loadZip(path, walkerId)
				{
					let zipFile = await yauzl.open(path);
					await zipFile.walkEntries( entry => {
						if(entry.compressedSize != 0 && lastWalkerId==walkerId)
							this.pushFile(new MediaFile(path, entry));
					});		
				}

				filesLoadFinished()
				{
					this.mediaFolder.loading = false;
					this.mediaFolder.files.sort(nodeUtil.sortFiles(scope.config.numericSorting));
					if(scope.config.enableFileHistory && this.depth !== 0)
						scope.storage.readerLastOpenDir = this.mediaFolder.path;

					let fileIndex = (scope.storage.readerFileHistory && scope.storage.readerFileHistory[this.mediaFolder.path] && scope.config.enableFileHistory) ? 
										parseInt(scope.storage.readerFileHistory[this.mediaFolder.path]) : 0;
					fileIndex = this.fileReference ? this.mediaFolder.files.findIndex( obj => obj.path == this.fileReference ) : fileIndex;
					// delete scope.openm.dir.fileReference;
					this.mediaFolder.setFileIndex(fileIndex);
					!scope.$root.$$phase && scope.$digest();
				}
			}

			// key functions
			mediaViewerFactory.keyBindFcts = 
			{
				changeFile: (x) => scope.openm.mediaFolder.setFileIndex(scope.openm.mediaFolder.fileIndex+parseInt(x)),
				changeSubFolder: (x) => 
				{
					let referencePath = scope.openm.mediaFolder.files[scope.openm.mediaFolder.fileIndex].zipPath || scope.openm.mediaFolder.files[scope.openm.mediaFolder.fileIndex].path;
					let pathList = [referencePath.substring(0, referencePath.lastIndexOf("/"))];
					let diffCounter = 0;
					for(let i = scope.openm.mediaFolder.fileIndex; (x>0 ? i<scope.openm.mediaFolder.files.length : i>0); (x>0 ? i++ : i--))
					{
						let filePath = scope.openm.mediaFolder.files[i].zipPath || scope.openm.mediaFolder.files[i].path;	
						if(!pathList.includes(filePath.substring(0, filePath.lastIndexOf("/"))))
						{
							diffCounter++;
							pathList.push(filePath.substring(0, filePath.lastIndexOf("/")));
						}
						if(diffCounter == (Math.abs(x) + (x>0 ? 0 : 1)))
						{
							scope.openm.mediaFolder.setFileIndex(i + (x>0 ? 0 : 1))
							return;
						}
					}
					if(x<0)
						scope.openm.mediaFolder.setFileIndex(0)
				},
				scrollVertical: (x) => mediaViewerFactory.handleScroll(0,parseFloat(x)),
				changeZoom: (x) => 					
				{
					mediaViewerFactory.changeZoom(parseFloat(x));
					mediaViewerFactory.displayOverlay();
				},
				toggleFullscreen: () => mediaViewerFactory.toggleFullScreen(),
				toggleSideMenu: () => scope.$apply(() => scope.config.hideSideMenu = !scope.config.hideSideMenu),
				toggleReader: () => scope.$apply(() => scope.config.show = !scope.config.show),
				scrollHorizontal:  (x) => mediaViewerFactory.handleScroll(parseFloat(x),0),
				deleteCurrentFile: () => 
				{
					let tempPath = scope.openm.mediaFolder.files.splice(scope.openm.mediaFolder.fileIndex,1)[0].src;
					scope.openm.mediaFolder.setFileIndex(scope.openm.mediaFolder.fileIndex, true);
					scope.openm.mediaFolder.setFilesLength();
					if(scope.openm.mediaFolder.files.length == 0)
						scope.$digest();
					electron.shell.moveItemToTrash(path.normalize(tempPath));
				},
				renameCurrentFile: async () =>
				{
					let newFileName = await renameFileModal(scope.openm.mediaFolder.files[scope.openm.mediaFolder.fileIndex].src);
					scope.openm.mediaFolder.files[scope.openm.mediaFolder.fileIndex].src = newFileName;
					scope.openm.mediaFolder.files[scope.openm.mediaFolder.fileIndex].path = newFileName;
					scope.openm.mediaFolder.setFileIndex(scope.openm.mediaFolder.fileIndex);
				},
				autoChangeFile: (x) =>
				{
					if(!scope.autoChangeFileInterval) 
					{	 	
						scope.openm.mediaFolder.setFileIndex(scope.openm.mediaFolder.fileIndex+1); 
						scope.autoChangeFileInterval = setInterval( () => scope.openm.mediaFolder.setFileIndex(scope.openm.mediaFolder.fileIndex+1), parseFloat(x) );
					}
					else
					{
						clearInterval(scope.autoChangeFileInterval);
						scope.autoChangeFileInterval = null;
					}
				},
				reloadApp: () => location.reload(),
				toggleDevTools: () => electronWindow.toggleDevTools(),
				customFunction: (x) => eval(x)
			}
			scope.keyBindFctNames = Object.keys(mediaViewerFactory.keyBindFcts);




			//ui help functions
			let loadBaseDir = async () =>
			{
				try
				{
					let dirPath = scope.config.basedir.replace(/\\/g,"/");
					let files = await nodeUtil.getFiles( dirPath[dirPath.length-1]=="/" ? dirPath.substring(0, dirPath.length-1) : dirPath );
					files.sort(nodeUtil.sortBaseNames);
					scope.rootdirs = scope.config.pinnedDirs.concat( files.filter( file => !scope.config.pinnedDirs.some(dir => dir.path == file.path) ) );
					scope.$digest();
					readerBaseDir.classList.remove("md-input-invalid");
				}
				catch(e)
				{
					readerBaseDir.classList.add("md-input-invalid");
				}
			}
			scope.baseDirGoUp = () =>
			{
				let newdir = scope.config.basedir.replace(/\\/g, "/");
				newdir = newdir.includes("/") ? newdir.substring(0,newdir.lastIndexOf("/")) : newdir;
				scope.config.basedir = !newdir.includes("/") ? newdir + "/" : newdir;
			}
			scope.baseDirOpenDialog = async () =>
			{	
				let response = await remote.dialog.showOpenDialog({properties: ['openDirectory']});
				scope.config.basedir = response.filePaths[0].replace(/\\/g, "/");
				scope.$digest();
			}
			
			scope.mdDialog = $mdDialog;
			let renameFileModal = async (oldPath) =>
			{
				scope.newFolderName = oldPath;
				scope.watchPathRename = true;
				await ngUtil.showDialog('#pathRename', null, null, () => scope.watchPathRename = false);
				await fs.move(oldPath, scope.newFolderName);
				return scope.newFolderName;
			}
			let deletePathModal = async (oldPath) =>
			{
				scope.newFolderName = oldPath.replace(/\\/g, "/");
				scope.watchPathDelete = true;
				await ngUtil.showDialog('#pathDelete', null, null, () => scope.watchPathDelete = false);
				scope.rootdirs.splice(scope.rootdirs.findIndex( (obj) => obj.path == scope.newFolderName ), 1);
				electron.shell.moveItemToTrash(path.normalize(scope.newFolderName));
			}
			scope.showReaderConfig = ($event) => {
				scope.watchReaderConfig = true;
				ngUtil.showDialog('#readerConfig', $event, null, () => scope.watchReaderConfig = false);
			};

			scope.setKeyString = (event,key) =>
			{
				event.preventDefault(); 
				key.type = mediaViewerFactory.getModifierString();
			}

			scope.deleteFileHistElem = (key) => delete scope.storage.readerFileHistory[key];
			scope.importFileHistory = async () =>
			{
				let response = await remote.dialog.showOpenDialog();
				scope.storage.readerFileHistory = await fs.readJSON(response.filePaths[0]);
				scope.$digest();
			};
			scope.exportFileHistory = () =>
			{
				let dlElem = document.createElement('a');
				dlElem.href = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(scope.storage.readerFileHistory,null,4));
				dlElem.download = "fileHistory.json";
				dlElem.click();
			};

			//listeners
			scope.$watch("config.basedir", (newVal, oldVal) => 
			{
				// fs.unwatchFile(oldVal);
				// fs.unwatchFile(newVal);
				// fs.watchFile(newVal, loadBaseDir);
				scope.config.basedir = scope.config.basedir.replace(/\\/g, "/");
				if(scope.config.basedir && newVal != oldVal)
					loadBaseDir();
			});
			$rootScope.$watch("$storage.reader.config.show", () => 
			{
				if(scope.config.show)
				{
					loadBaseDir();
					if(scope.openm && !scope.openm.mediaFolder.path && scope.openm.mediaFolder.files.length==0 && scope.config.enableFileHistory && scope.storage.readerLastOpenDir)
						scope.openm.loadPath(scope.storage.readerLastOpenDir);
				}
			});


			let dirNotification = document.getElementById("dirNotification");
			let getDirElem = (elem) => 
			{
				while(elem && !elem.getAttribute("dirPath"))
					elem = elem.parentElement;
				return elem;
			}
			// window.handleDirOnClick = (ev) => scope.openm.loadPath(getDirElem(ev.target).getAttribute("dirPath"));
			window.handleDirDragStart = (ev) => ev.dataTransfer.setData("dirPath", getDirElem(ev.target).getAttribute("dirPath"));
			window.handleDirDragOver  = (ev) => {
				ev.dataTransfer.dropEffect = 'copy';
				getDirElem(ev.target).style.borderColor = "#dddddd";	
			}
			window.handleDirDragLeave = (ev) => getDirElem(ev.target).style.borderColor = "transparent";
			window.handleDirDrop = async (ev) =>
			{
				getDirElem(ev.target).style.borderColor = "transparent";
				let targetDir = getDirElem(ev.target).getAttribute("dirPath");
				let sourceDir = ev.dataTransfer.getData("dirPath");
				let uriList = ev.dataTransfer.getData("text/uri-list") || mediaViewerFactory.videoDragPath;
				if(ev.dataTransfer.files[0])
				{
					if(path.extname(ev.dataTransfer.files[0].path))
						uriList = ev.dataTransfer.files[0].path;
					else
						sourceDir = ev.dataTransfer.files[0].path;
					ev.stopPropagation();
				}
				if(path.extname(targetDir) || targetDir == sourceDir)
					return;
				if(sourceDir)
				{	
					await fs.rename(sourceDir, targetDir + "/" + path.basename(sourceDir));
					loadBaseDir();
				}
				if(uriList)
					for(let i=0; true; i++)
					{
						uriList = decodeURI(uriList).replace("file:///","")
						let filePath = targetDir + "/" + path.basename(uriList).split(".")[0] + (i==0 ? "" : " ("+i+")") + "." + path.basename(uriList).split(".")[1];
						if(!await fs.exists(filePath))
						{	
							await fs.copy(uriList, filePath);
							$mdToast.show($mdToast.simple()
								.textContent('✓ copied file')
								.hideDelay(2000)
								.toastClass("centeredToast")
								.parent(dirNotification));
							break;
						}
					}
			}
			document.addEventListener('dragover', (ev) =>
			{
				ev.preventDefault();
				if(ev.dataTransfer.files.length > 0)
					ev.dataTransfer.dropEffect = 'move';
			});
			document.addEventListener('drop', (ev) =>
			{
				ev.preventDefault();
				mediaViewerFactory.videoDragPath = undefined;
				if(ev.dataTransfer.files[0] && ev.dataTransfer.effectAllowed != "copyLink")
				{
					scope.openm.loadPath(ev.dataTransfer.files[0].path);
					scope.$apply(() => scope.config.show = true);
				}
			});



			document.addEventListener('focus', (ev)=>
			{				
				if(ev.target.classList[1] == "md-button" || ev.target.classList[2] == "md-button" || ev.target.classList[1] == "reader-ui-icons")
				{
					document.activeElement.blur();
					ev.preventDefault();
					ev.stopPropagation();
				}
			}, true);

			let processArgs = (folderArgs) =>
			{
				let folderArg = folderArgs.slice(-1)[0];
				if(folderArgs.length>1 && folderArg[0] != "-" && folderArg[1] != "-" && folderArg != ".")
				{
					scope.config.show = true
					scope.openm.loadPath(folderArg);
					return true;
				}
			}
			let secondInstanceHandler = (event, commandLine, workingDirectory) => 
			{
				if($rootScope.preConfig.singleInstanceLock)
				{
					electronWindow.focus();
					scope.$apply(() => processArgs(commandLine));
				}
			}
			electronApp.on('second-instance', secondInstanceHandler);
			window.addEventListener('beforeunload', () => electronApp.removeListener('second-instance', secondInstanceHandler) );
		}
    };
}]);
	