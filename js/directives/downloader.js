let MmodelWorker = isPackaged ? require('worker-loader!js/workers/mmodel.js') : Worker;
let FileSaveWorker = isPackaged ? require('worker-loader!js/workers/fileSave.js') : Worker;
let PackingWorker = isPackaged ? require('worker-loader!js/workers/packing.js') : Worker;



angular.module("downloader", ['ngMaterial']).directive("downloaderUi",
['$rootScope', '$q', 'mmShared', 'ngUtil', 'testFactory',	
( $rootScope ,  $q ,  mmShared ,  ngUtil ,  testFactory) =>
{
    return {
		restrict : 'E',
        scope: {},
        templateUrl : 'js/directives/downloader.html',
		link: async function($scope)
		{
			// TODO code: change worker library; use youtube-dl to download videos? cordova port; queue based main loop;
			// TODO fcts: more convenient update process (+ delta update);
			// TODO   ui: rewrite chapter selection (performance); 
			// TODO  doc:  download pattern; better api docs;

			//init
			debugLog($scope);

			setTimeout( () => $rootScope.$watch("$storage.reader.config.show", () => !$rootScope.$storage.reader.config.show ? $scope.$resume() : $scope.$suspend()));

			let throttledDigest500   = ngUtil.throttle(function(){ if(!$rootScope.$storage.reader.config.show) $scope.$digest(); },500);
			let throttledDigest10000 = ngUtil.throttle(function(){ if(!$rootScope.$storage.reader.config.show) $scope.$digest(); },10000);
			$scope.ngUtil = ngUtil;
			$scope.userThemes = userThemes;
			$scope.editUserTheme = Object.keys(userThemes)[0];
			$scope.config = $rootScope.$storage.config;

			$scope.mlists = {};
			$scope.currentm = {};
			$scope.currentmlist = {};
			$scope.filteredMlist = [];
			$scope.mmodels = [];

			$scope.mediaTypes = 
			{
				"anime":   { volPrefix: 'S.',   chaPrefix: 'Ep.'  },
				"comic":   { volPrefix: 'Vol.', chaPrefix: 'Cha.' },
				"gallery": { volPrefix: 'Vol.', chaPrefix: 'Cha.' },
				"manga":   { volPrefix: 'Vol.', chaPrefix: 'Cha.' }
			}
			$scope.packFormats = ["raw", "cbz", "pdf", "epub"];
			$scope.packMenu = [];
			for(let packFormat of $scope.packFormats.slice(1, $scope.packFormats.length))
				$scope.packMenu.push({
					text: 'pack as ' + packFormat,
					click: function ($itemScope, $event, modelValue, text, $li) {
						$itemScope.dl.packMedia(text[0].innerText.split("pack as ")[1]);
					}
				});


			if(!fs.existsSync(userPaths.mlists))
				await fs.ensureDir(userPaths.mlists);

			// load the mmodels
			let fetchRemoteScripts = async (loadBasePath) =>
			{
                let remoteScripts = await fetch("https://gitlab.com/api/v4/projects/3342389/repository/tree?path=js%2Fmmodels&ref=master&recursive=true&per_page=9999");
                remoteScripts = await response.json();
				remoteScripts = remoteScripts.data.map(resp => {return resp.path.split("js/mmodels/")[1];}).filter(str => {return str.split(".")[1];});
	
				await fs.remove(loadBasePath);
				await fs.ensureDir(loadBasePath);

				let promises = [];
                for(let script of remoteScripts)
                {
                    let remoteScripts = await fetch("https://gitlab.com/CopyNippyJoin/emdl/raw/master/js/mmodels/" + script);
                    remoteScripts = await response.text();
                    await fs.writeFile(loadBasePath + script.split("/").splice(-1)[0], remoteScripts);
                }
			}
			let loadMmodels = async () =>
			{
				let mmodels = [];
				let loadBasePath = userPaths.mmodels;
				if($scope.config.fetchRemoteScript)
				{
					loadBasePath = userPaths.remoteMmodels;
					await fetchRemoteScripts(loadBasePath);
				}

				let files = (await nodeUtil.getFilesRecursive(loadBasePath)).filter( file => (!file.includes("-worker") && file.includes(".js")) );
				for(let file of files)
				{
					mmodels.push(nodeRequire(file));
					mmodels[mmodels.length-1].path = file.replace(/\\/g,"/");
				}			
			
				mmodels = mmodels.sort(ngUtil.sortByAttr('name'));
				$scope.currentmmodel = mmodels[0];
				let currentmmodelname = $scope.config.currentmmodelname;
		
				for(let mmodel of mmodels)
				{
					if(currentmmodelname == mmodel.name)
						$scope.currentmmodel = mmodel;
					if(!currentmmodelname && "mangaseeonline" == mmodel.name)
						$scope.currentmmodel = mmodel;
		
					if($scope.config.mmodeloptions[mmodel.name])
					{
						ngUtil.addMissingProps(mmodel.options, $scope.config.mmodeloptions[mmodel.name]);
						mmodel.options = $scope.config.mmodeloptions[mmodel.name];
					}
					else
						$scope.config.mmodeloptions[mmodel.name] = mmodel.options;
				}
				
				$scope.mmodels = mmodels;

				if(!isPackaged)
				{
					$scope.testFactory = testFactory;
					testFactory.mmodels = mmodels;
				}
				$scope.$digest();
			}
			loadMmodels();

		
			//ui help functions
			$scope.chapterPreview = [ ['Preview', ($itemScope, $event, modelValue, text, $li) => $scope.downloads.addDL(modelValue)] ];
			$scope.filterMlist = () =>
			{
				if($scope.currentmlist && $scope.currentmlist.cont)
				$scope.filteredMlist = $scope.currentmlist.cont.filter( (obj) => obj.name.toLowerCase().includes($scope.config.search.toLowerCase()) );
			}

			$scope.openThemePath = () => ngUtil.openExternal(userPaths.themes);
			$scope.saveMmodelOptions = () => $scope.config.mmodeloptions[$scope.currentmmodel.name] = $scope.currentmmodel.options;

			
			$scope.showGlobalConfig = ($event) => {
				$scope.watchGlobalConfig = true;
				ngUtil.showDialog('#globalConfig', $event, null, () => $scope.watchGlobalConfig = false);
			};
			$scope.showDownloadConfig = ($event) => {
				$scope.watchDownloadConfig = true;
				ngUtil.showDialog('#downloadConfig', $event, null, () => $scope.watchDownloadConfig = false);
			};
			$scope.dlDirOpenDialog = async () =>
			{	
				let response = await remote.dialog.showOpenDialog({properties: ['openDirectory']});
				$scope.config.dldir = response.filePaths[0].replace(/\\/g, "/");
				$scope.$digest();
			}
			$scope.getSelectedChapters = (chapters = []) =>
				chapters.filter( chapter => (chapter.selected && chapter.version==$scope.currentm.curversion) );
			$scope.toggleChaptersExpand = (chapters, reference) =>
				chapters.forEach( chapter => {if(reference.volnumb==chapter.volnumb && reference.version==chapter.version) chapter.expand = !chapter.expand} );

			$scope.toggleChapterSelection = (chapters,chapter,isvol,length) =>
			{
				chapters = chapters.filter( cha => chapter.volnumb==cha.volnumb && chapter.version==cha.version);
				let from = chapters.indexOf(chapter);
				let to = length ? Math.min(from + length, chapters.length) : chapters.length;
				for(; from<to; from++)
					chapters[from].selected = isvol ? chapter.volsel : !chapters[from].selected;

				let volchap = chapters.find( cha => cha.newvol );
				volchap.volsel = chapters.some( cha => cha.selected );
			}

			$scope.removeFileExtension = nodeUtil.removeFileExtension;
			$scope.addUserTheme = () =>
			{
				let key = "newTheme.json";
				for(let i=0; $scope.userThemes[key]; i++)
					key = "newTheme ("+(i+1)+").json";
				$scope.editUserTheme = key;
				$scope.userThemes[key] = {"50": "e2e7e8","100": "b6c2c5","200": "869a9e","300": "557177","400": "30525a","500": "0c343d","600": "0a2f37","700": "08272f","800": "062127","900": "03151a","A100": "58cfff","A200": "25c1ff","A400": "00acf1","A700": "009ad8"};
			}
			$scope.renameTheme = async ($event) =>
			{
				let themeData = JSON.parse(JSON.stringify(($scope.userThemes[$scope.editUserTheme])));
				$scope.deleteCurrentTheme();
				$scope.editUserTheme = $event.target.value + ".json"
				$scope.userThemes[$scope.editUserTheme] = themeData;
				$scope.saveCurrentTheme();
			}
			$scope.saveCurrentTheme = ngUtil.debounce(() => { fs.writeJSON(userPaths.themes+$scope.editUserTheme, $scope.userThemes[$scope.editUserTheme]); });
            $scope.deleteCurrentTheme = () => 
			{
				fs.remove(userPaths.themes+$scope.editUserTheme);
				delete $scope.userThemes[$scope.editUserTheme];
				$scope.editUserTheme = Object.keys(userThemes)[0];
			}
			window.setThemeColor = (event) => 
			{
				$scope.userThemes[$scope.editUserTheme][event.target.getAttribute("colorKey")] = event.target.value.replace("#", "");
				$scope.$digest();
				let newPalette = {};
				for(let key in $scope.userThemes[$scope.editUserTheme])
					newPalette[key] = {hex: $scope.userThemes[$scope.editUserTheme][key], value: nodeUtil.hexToRgb($scope.userThemes[$scope.editUserTheme][key])};

				let themeId = nodeUtil.removeFileExtension($scope.editUserTheme);
				ngUtil.themeProvider.definePalette(themeId, newPalette);
				if(!ngUtil.colorPalettes.some( (id) => id == themeId))
					ngUtil.colorPalettes.push(themeId);
				ngUtil.colorPalettes.sort(nodeUtil.sortFilesHelper.sortFilesHelper);
				if($scope.config.colorTheme.accentPalette     == themeId ||
				   $scope.config.colorTheme.backgroundPalette == themeId ||
				   $scope.config.colorTheme.primaryPalette    == themeId ||
				   $scope.config.colorTheme.warnPalette       == themeId)
					$scope.$apply(ngUtil.updateColorTheme);
				$scope.saveCurrentTheme();
			}
		
			ngUtil.mlistOnProgress = (progress) =>
			{
				$scope.currentmlist.progress = progress;
				throttledDigest10000();
			}
			$scope.getMangas = async function(forcenew)
			{
				if($scope.currentmmodel.options.livesearch)
					return;
			
				let mmodelName = $scope.currentmmodel.name;
				let mlistPath = path.join(userPaths.mlists, mmodelName + ".json");
				if(!$scope.mlists[mmodelName] && await fs.pathExists(mlistPath))
					$scope.mlists[mmodelName] = await fs.readJson(mlistPath);
				$scope.currentmlist = {progress:100, cont:$scope.mlists[mmodelName], mmodelname:mmodelName};

				if(forcenew || !$scope.currentmlist.cont)
				{
					try
					{
						$scope.currentmlist.progress = 0;
						$scope.currentmlist.cont = [];
						$scope.filterMlist();
						let newMlist = await $scope.currentmmodel.mextract(mmShared);
						if(!newMlist||newMlist.length==0)
							throw new Error('mlist array is empty');
						$scope.currentmlist.cont = newMlist.sort(ngUtil.sortByAttr('name'));
						$scope.currentmlist.progress = 100;
						
						if(mmodelName == $scope.currentmmodel.name)
						{
							$scope.mlists[mmodelName] = $scope.currentmlist.cont;
							await fs.writeFile(mlistPath, JSON.stringify($scope.currentmlist.cont), 'utf8');
						}
					}
					catch(e)
					{
						$scope.currentmlist.progress = 100;
						console.error({"desc":"failed to extract the manga list for mmodel "+mmodelName,"method":"mextract","receiveddata":e});
					}
				}
				
				$scope.filterMlist();
				!$rootScope.$$phase && $scope.$digest();
			}


			let sortChapters = (a, b) =>
			{
				if(a.version > b.version) return  1;
				if(a.version < b.version) return -1;
				if(a.volnumb > b.volnumb) return  1;
				if(a.volnumb < b.volnumb) return -1;
				if(a.chanumb > b.chanumb) return  1;
				if(a.chanumb < b.chanumb) return -1;
				if(a.numb    > b.numb   ) return  1;
				if(a.numb    < b.numb   ) return -1;
				return -1;
			};
			$scope.getChapters = async function(m)
			{
				$scope.currentm = JSON.parse(JSON.stringify(m));
				$scope.currentm.name = $scope.currentm.name || "undefined";
				
				try
				{
					let chapters = await $scope.currentmmodel.cextract(mmShared, m);
					if(!chapters||chapters.length==0)
						throw new Error('chapters array is empty');
					chapters = mmShared.normalizeChapterNumbers(chapters);
					
					chapters.sort(sortChapters);
					chapters[0].newvol = true;
					chapters[0].volsel = true;
					chapters[0].chadiv = true;
					chapters[0].selected = true;
					let k = 1;
					for(let i=1; i<chapters.length; i++)
					{
						chapters[i].selected = true;
						if(k%5==0)
							chapters[i].chadiv = true;
						if(chapters[i-1].volnumb!=chapters[i].volnumb || chapters[i-1].version!=chapters[i].version)
						{
							chapters[i].newvol = true;
							chapters[i].volsel = true;
							chapters[i].chadiv = true;
							k=0;
						}
						k++;
					}
		
					let versions =  [...new Set(chapters.map( cha => cha.version).filter(Boolean))].sort();
					if(versions.length>0)
					{
						$scope.currentm.curversion = versions[0];
						$scope.currentm.versions = versions;
					}
		
					$scope.currentm.chapters = chapters;
				}
				catch(e)
				{
					$scope.currentm.name = "failed to retrieve any chapter for your input";
					$scope.currentm.error = true;
					console.warn({"cause":e,"method":"cextract","input":m});
				}
				$scope.$digest();
			}
			$scope.getLiveSearchChapters = (name) =>
			{
				let m = {name:name||$scope.config.search, link:name||$scope.config.search};
				if($scope.currentmmodel.getMlink)
					m.link = $scope.currentmmodel.getMlink(m);
				$scope.getChapters(m);
			}

			window.addEventListener('beforeunload', () =>
			{
				for(let download of $scope.downloads.list)
					download.terminateWorkers();
			});

			class Downloads 
			{
				constructor()
				{
					this.list = [];
				}

				addDL(previewChapter)
				{
					if(!$scope.currentm||!$scope.currentmmodel)
						return;
	
					$scope.currentm.chapters = previewChapter && previewChapter!="selected" ? [previewChapter] : $scope.getSelectedChapters($scope.currentm.chapters);
					this.list.push(new Download($scope.currentm, $scope.currentmmodel, $scope.config, Boolean(previewChapter)));
					$scope.currentm = {};
					this.checkState();
				}
				removeDL($index)
				{
					this.list[$index].terminateWorkers();
					this.list[$index].terminated = true;
					this.list.splice($index, 1);
					this.checkState();
				}

				sort()
				{
					this.list.sort((a,b) =>
					{
						if(a.state == b.state)
							return 0;
						if( a.state=="running" ||
						   (a.state=="queued" && b.state!="running") ||
						   (a.state=="paused" && b.state!="running" && b.state!="queued"))
							return -1;
						return 1;
					});
				}

				checkState()
				{
					this.sort();
					for(let download of this.list)
						if(download.state=="queued" && (download.mmodel.options.latency<=0 ||
								!this.list.some( reference => download.mmodel.name==reference.mmodel.name && reference.state=="running") ))
							download.start();		
				}
			}
			$scope.downloads = new Downloads();

			class Download
			{
				constructor(m, mmodel, config, preview)
				{
					this.manga = JSON.parse(JSON.stringify(m));
					this.manga.chapters.forEach( (chapter) => chapter.skipped = true );
					this.manga.completedPages = [];
					this.requestPredict = {chapterCounter:0, pagesAccu:0, saveFailedCounter:0};
					this.mmodel = mmodel;
					this.mmodel.concurrencyLevel = this.mmodel.options.concurrencyLevel || "afterIextract";
					this.state = "queued";
					this.progress = 0;
					this.preview = preview;
					this.format = config.format;
					this.basePath = config.dldir.replace(/\\/g,"/");
					this.patternPath = config.dlreppath.replace(/\%MediaTitle\%/g, m.name.replace(/[\/\\]/g,'-')
																					       .replace(/[^A-Za-z0-9 \, \' \# \; \- \_ \( \) \+ \! \&]/g,'')
																					       .replace(/(^\s+|\s+$)/g, ''));
				}

				async reset()
				{
					this.i = 0;
					this.j = 0;
					if(this.keepProgress)
					{
						this.completed = this.completed - this.requestPredict.saveFailedCounter
						this.requested = this.completed;
					}
					else
					{
						this.requested = 0;
						this.completed = 0;			
						this.requestPredict = {chapterCounter:0, pagesAccu:0};
					}
					this.requestPredict.saveFailedCounter = 0;
					this.keepProgress = false;

					if(!this.downloadWorker)
						this.downloadWorker = new MmodelWorker("js/workers/mmodel.js");
					await nodeUtil.getThreadResult(this.downloadWorker, {"method":"setOptions",
						"options": this.mmodel.options, "paths":{ "mmodel":this.mmodel.path } });

					if(!this.fileSaveWorker)
						this.fileSaveWorker = new FileSaveWorker("js/workers/fileSave.js");
				}

				getProgress()
				{
					if(this.progress == "packing")
						return this.progress;
					return parseFloat(this.progress).toFixed(2) + "%";
				}

				async start()
				{
					this.state = "running";
					await this.reset();
					this.removeCompletedPages();
					$scope.downloads.sort();
					setTimeout(() => this.getPages(), this.mmodel.latency);
				}

				removeCompletedPages()
				{
					for(let i=0;i<this.manga.chapters.length;i++)
						if(this.manga.chapters[i].pages)
						{
							let completedPages = this.manga.chapters[i].pages.filter( (page) => !page.skipped );
							completedPages.forEach(page => 
							{
								page.chanumb = this.manga.chapters[i].chanumb;
								page.volnumb = this.manga.chapters[i].volnumb;
							});
							this.manga.completedPages = this.manga.completedPages.concat(completedPages);
							this.manga.chapters[i].pages = this.manga.chapters[i].pages.filter( (page) => page.skipped );
						}
					let skipCheck = this.manga.chapters.filter( (chapter) => (chapter.skipped || (chapter.pages && chapter.pages.length)) );
					this.skipped = skipCheck.length == 0 ? false : true;

					this.manga.completedPages.forEach(page => 
					{
						try{ page.chanumb = parseFloat(page.chanumb); } catch (e) { console.log(e); }
						try{ page.volnumb = parseFloat(page.volnumb); } catch (e) { console.log(e); }
						try{ page.numb    = parseFloat(page.numb);    } catch (e) { console.log(e); }
					});
					this.manga.completedPages.sort(sortChapters);
				}
				terminateWorkers()
				{
					this.downloadWorker && this.downloadWorker.terminate();
					this.fileSaveWorker && this.fileSaveWorker.terminate();
					this.downloadWorker = null;
					this.fileSaveWorker = null;
				}

				getEntryFolder()
				{
					for(let chapter of this.manga.chapters)
						if(chapter.patternPath)
							return this.basePath + "/" + chapter.patternPath.replace(this.basePath,"").split("/").filter(Boolean)[0];
				}
				async packMedia(format)
				{
					if(format == "raw")
						return;
					this.progress = "packing";
					!$rootScope.$$phase && $scope.$digest();
					let input = {"entryFolder": this.getEntryFolder(), "format": format, "media": this.manga};
					let packWorker = new PackingWorker("js/workers/packing.js");
					await nodeUtil.getThreadResult(packWorker, input);
					packWorker.terminate();
					this.progress = 100;
					$scope.$digest();
				}
				async end()
				{
					if(this.state!="running" || this.completed!=this.requested || this.i<this.manga.chapters.length)
						return;
					
					this.state = "completed";
					setTimeout( () => this.terminateWorkers());
					$scope.downloads.checkState();

					if(this.preview)
					{
						ngUtil.loadPreview(this.manga.chapters.reduce( (accu, cha) => accu = accu.concat(cha.pages) , []));
						this.removeCompletedPages();
						return;
					}
					
					this.removeCompletedPages();
					$scope.$digest();

					let entryFolder = this.getEntryFolder();
					if(!entryFolder)
						console.warn({"desc":"all chapters of " + this.manga.name + " were skipped"});
					else
					{
						let fileList = (await nodeUtil.getFilesRecursive(entryFolder)).map(function(path){ return {path:path}; });
						console.info("Download of " + this.manga.name + " finished.\nRequested " + this.requested +
									 " files and found " + fileList.length + " files in the folder " + entryFolder);
						await this.packMedia(this.format);
					}
				}

				toggleState()
				{
					switch(this.state)
					{
						case 'running':
							this.state = 'paused';
							break;
						case 'queued':
							this.state = 'paused';
							break;
						case 'paused':
							this.state = 'queued';
							this.keepProgress = true;
							break;
						case 'completed':
							if(this.skipped)
							{
								this.state = "queued";
								this.keepProgress = true;
							}
							break;
					}
					$scope.downloads.checkState();
				}

				static errorLog(method, dl, i, j, error)
				{
					let volnumb = dl.manga.chapters[i] ? dl.manga.chapters[i].volnumb : undefined;
					let chanumb = dl.manga.chapters[i] ? dl.manga.chapters[i].chanumb : undefined;
					let pages = isNaN(j) ? "" : " p." + (j+1);
					console.warn({"method":method, "source":dl.manga.name+" v."+ volnumb +" ch."+ chanumb + pages, "cause":error, "input":dl.manga.chapters[i]});
				}
				downloadNext(level)
				{
					let dl = this;
					if(dl.terminated)
						return;

					if(level == "chapter")
						setTimeout(function(){dl.getImages();}, dl.mmodel.options.latency);
					if(level == "pages")
						setTimeout(function()
						{
							dl.j++;
							dl.getImages();
						}, Math.max(dl.mmodel.options.latency - (performance.now() - dl.timereq),0));
				}
				async getPages()
				{
					let dl = this;
					if(dl.manga.chapters[dl.i].pages)
					{
						dl.getImages();
						return;
					}
	
					try
					{
						dl.manga.chapters[dl.i].skipped = true;
						let response = await nodeUtil.getThreadResult(dl.downloadWorker,{"method":"pextract", "curchap": dl.manga.chapters[dl.i]});
						if(!response.data || response.data.length==0)
							throw new Error('pages array is empty');
						else
						{
							dl.manga.chapters[dl.i].skipped = false;
							dl.manga.chapters[dl.i].pages = response.data;
							dl.manga.chapters[dl.i].pages.forEach( (page) => page.skipped = true );
							dl.manga.chapters[dl.i].patternPath = dl.patternPath
																    .replace(/\%Volume\%/g,dl.manga.chapters[dl.i].volnumb)
																    .replace(/\%Chapter\%/g,dl.manga.chapters[dl.i].chanumb)
																    .replace(/\%MModelName\%/g,dl.mmodel.name)
																    .replace(/\%Version\%/g,dl.manga.chapters[dl.i].version)
																    .replace(/\\/g,"/");
							dl.requestPredict.pagesAccu += dl.manga.chapters[dl.i].pages.length;
							dl.requestPredict.chapterCounter++;
						}
					}
					catch(e)
					{
						dl.manga.chapters[dl.i].pages = null;
						Download.errorLog("pextract", dl, dl.i, undefined, e);
					}
					finally
					{
						dl.downloadNext("chapter");
					}
					
				}

				async getImages()
				{
					let dl = this;
					if(dl.state!="running")
						return;
						
					if(!dl.manga.chapters[dl.i].pages||dl.j>=dl.manga.chapters[dl.i].pages.length)
					{
						dl.i++;
						dl.j = 0;
						if(dl.i<dl.manga.chapters.length)
							dl.getPages();
						else
							dl.end();				
						return;
					}
					
					(async function (dli,dlj)
					{
						dl.mmodel.concurrencyLevel == "beforeIextract" && dl.downloadNext("pages");
						dl.timereq = performance.now();
						dl.manga.chapters[dli].pages[dlj].skipped = true;

						let response = undefined;
						try
						{
							if(!dl.manga.chapters[dli].pages[dlj].src)
							{
								response = await nodeUtil.getThreadResult(dl.downloadWorker,{"method":"iextract", "curpage": dl.manga.chapters[dli].pages[dlj]});
								if(!response.data || !response.data.src)
									throw new Error('got empty image source');	
							}
						}
						catch(e)
						{
							Download.errorLog("iextract", dl, dli, dlj, e);
							return;
						}
						finally
						{
							dl.mmodel.concurrencyLevel == "afterIextract" && dl.downloadNext("pages");
						}
						if(!dl.manga.chapters[dli].pages[dlj].src)
							dl.manga.chapters[dli].pages[dlj] = Object.assign(dl.manga.chapters[dli].pages[dlj], response.data || {});
						dl.requested++;					
	
						try
						{
							if(dl.preview)
								dl.manga.chapters[dli].pages[dlj].skipped = false;
							else
							{
								dl.manga.chapters[dli].pages[dlj].targetPath = dl.basePath + "/" + dl.manga.chapters[dli].patternPath.replace(/\%FileName\%/g,dl.manga.chapters[dli].pages[dlj].numb);
								let response = await nodeUtil.getThreadResult(dl.fileSaveWorker,{"page": dl.manga.chapters[dli].pages[dlj], 
									"i":dli, "j":dlj, "mmodelPath": dl.mmodel.options.removeWatermarks ? dl.mmodel.path : null});
								dl.manga.chapters[response.input.i].pages[response.input.j].skipped = false;
								dl.manga.chapters[response.input.i].pages[response.input.j].targetPath = response.input.page.targetPath;
							}
						}
						catch(e)
						{	
							dl.requestPredict.saveFailedCounter++;
							Download.errorLog("mediaSaveWorker", dl, e.input ? e.input.i : undefined,  e.input ? e.input.j : undefined, e);
						}
						finally
						{
							dl.completed++;
							dl.mmodel.concurrencyLevel == "afterMediaSave" && dl.downloadNext("pages");
							let guess = (dl.requestPredict.pagesAccu/dl.requestPredict.chapterCounter) * (dl.manga.chapters.length - dl.requestPredict.chapterCounter);
							dl.progress = 100*((dl.completed - dl.requestPredict.saveFailedCounter)/( dl.requestPredict.pagesAccu + guess));
							throttledDigest500();
							dl.end();
						}
					})(dl.i,dl.j);
				}
			}
		
			//watchers
			$scope.$watch("currentmmodel", async function(newValue, oldValue)
			{
				if(!$scope.currentmmodel)
					return;
				
				$scope.ngUtil.currentmmodel = $scope.currentmmodel;
				$scope.currentm={};
				$scope.mlistContainerIndex = 0;
				
				if(oldValue)
					oldValue.cancelMlist = true;
				$scope.currentmmodel.cancelMlist = false;
				
				$scope.getMangas();
				$scope.config.currentmmodelname = $scope.currentmmodel.name;
			});	
		}
    };
}]);
	