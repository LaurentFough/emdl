const mime = require('mime-types');

let mediaViewer = angular.module('mediaViewer', ['ngMaterial']);
mediaViewer.factory('mediaViewerFactory',
() =>
{
	let factory = {}; 
	factory.overlay = { 
		left:[], 
		center:[
			`<i class="material-icons reader-ui-icons" ng-mousedown="uiMouseDownFileIndex(-25);">&#xE5DC;</i>`,
			`<i class="material-icons reader-ui-icons" ng-mousedown="uiMouseDownFileIndex(-1);">&#xE5CB;</i>`,
			`<div style="padding: 0px 8px;">
				<span class="readerUiFileNumbers" id="fileIndex"  ></span> / 
				<span class="readerUiFileNumbers" id="filesLength"></span>
			</div>`,
			`<i class="material-icons reader-ui-icons" ng-mousedown="uiMouseDownFileIndex(1);">&#xE5CC;</i>`,
			`<i class="material-icons reader-ui-icons" ng-mousedown="uiMouseDownFileIndex(25);">&#xE5DD;</i>`
		], 
		right:[
			`<div style="padding: 0px 8px;">x{{config.zoomFac.toFixed(1)}}</div>`,
			`<i class="material-icons reader-ui-icons" ng-mousedown="uiMouseDownZoom(-0.1);">&#xE900;</i>`,
			`<i class="material-icons reader-ui-icons" ng-mousedown="uiMouseDownZoom( 0.1);">&#xE8FF;</i>`,
			`<i class="material-icons reader-ui-icons" id="fullscreenButton" ng-click="toggleFullScreen();">
				{{isFullScreen?'&#xE5D1;':'&#xE5D0;'}}
			</i>`
		] 
	};
	factory.config = {};

	class MediaFile
	{
		constructor(path, zipFile)
		{
			this.path = path;
			this.zipFile = zipFile;
			this.zipPath = zipFile ? "/" + zipFile.fileName : undefined;
			this.mimeType = zipFile ? mime.lookup(zipFile.fileName) : mime.lookup(path);
			this.src = zipFile ? "" : path.replace(/\#/g, "%23");
			this.mediaType = this.getMediaType();
		}

		getMediaType()
		{
			let mime = this.mimeType;
			if(mime=="application/pdf")
				return "pdf";
			if(mime=="application/x-cbr"||mime=="application/zip")
				return "zip";
			mime = mime ? mime.split('/')[0] : "";
			return mime == "audio" ? "video" : mime;
		}
	}
	factory.MediaFile = MediaFile;

    return factory;
});

mediaViewer.directive("mediaViewer",
["$compile", "mediaViewerFactory",
( $compile ,  mediaViewerFactory) =>
{
    return {
		restrict : 'E',
		scope: {},
        templateUrl : 'js/directives/mediaViewer.html',
		link: function(scope, element, attrs, controllers)
		{
			debugLog(scope, mediaViewerFactory);

			scope.$watch(() => mediaViewerFactory.config, () => scope.config = mediaViewerFactory.config);
			let initMediaViewer = () =>
			{
				setTimeout( () =>
				{
					scope.openm = mediaViewerFactory.openm = new MediaFolder();
					for(let key in fileTypeAdapters)
						fileTypeAdapters[key].init && fileTypeAdapters[key].init();
					mediaViewerFactory.setZoom(scope.config.zoomFac); // prevents application of zoom to video controls
				});
			}

			let domElems = {};
			let addDomElems = (domIds) =>
			{		
				for(let domId of domIds)
					domElems[domId] = document.getElementById(domId);
			}
			addDomElems(["viewerContainer", "videoDisplay", "imageDisplay", "pdfDisplay",
				"viewerOverlay", "leftOverlay", "centerOverlay", "rightOverlay"]);
			scope.setScrollContainer = () => domElems.scrollContainer = document.getElementById("scrollContainer");
			domElems.imagePrefetch = [{img:new Image(), offset:-1, decode:false}, {img:new Image(), offset:1, decode:true}];

			let fileTypeAdapters = {
				"image": {
					"change": (currentFile) =>
					{
						// domElems.imageDisplay.onload = () => { }
						domElems.imageDisplay.src = currentFile.src;
						domElems.imageDisplay.style.display = "block";
					},
					"leave": () => domElems.imageDisplay.style.display = "none"
				},
				"pdf": {
					"change": (currentFile) =>
					{
						domElems.pdfDisplay.src = currentFile.src + "#toolbar=0";
						domElems.pdfDisplay.style.display = "block";
						scope.$digest();
					},
					"leave": () => {
						domElems.pdfDisplay.style.display = "none";
						domElems.pdfDisplay.src = "";
					} 
				},
				"video": {
					"init": () => 
					{
						mediaViewerFactory.videoPlayer = new Plyr('#videoDisplay', {
							"clickToPlay": false,
							"keyboard": { "focused": false, "global": false },
							"controls": ['play', 'mute', 'volume', 'current-time', 'duration', 'progress', 'captions', 'settings', 'fullscreen'],
							"speed": { "options": [0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2] }
						});

						mediaViewerFactory.videoPlayer.autoplay = scope.config.videoAutoPlay;
						mediaViewerFactory.videoPlayer.loop = scope.config.videoLoop;
						scope.$watch("config.videoAutoPlay", () => mediaViewerFactory.videoPlayer.autoplay = scope.config.videoAutoPlay );
						scope.$watch("config.videoLoop", () => mediaViewerFactory.videoPlayer.loop = scope.config.videoLoop );

						mediaViewerFactory.videoPlayer.elements.container.style.display = "none";
						mediaViewerFactory.videoPlayer.elements.container.setAttribute("draggable", true);
					},
					"change": (currentFile) =>
					{
						mediaViewerFactory.videoPlayer.eventListeners.forEach(item => {
							const { element, type, callback, options } = item;
							if(type == "mousemove")
								return;
							element.removeEventListener(type, callback, options);
						});
						mediaViewerFactory.videoPlayer.eventListeners = [];
						mediaViewerFactory.videoPlayer.source = {
							type: 'video',
							sources: [{
								"src": currentFile.src,
								"type": currentFile.mimeType,
							}]
						};
						mediaViewerFactory.videoPlayer.elements.container.ondragstart = (event) => 
						{
							event.preventDefault();
							let canvas = document.createElement('canvas');
							let videoElement = mediaViewerFactory.videoPlayer.media;
							canvas.height = videoElement.videoHeight*(150/videoElement.videoWidth);
							canvas.width = 150;
							canvas.getContext('2d').drawImage(videoElement, 0, 0, videoElement.videoWidth, videoElement.videoHeight, 0, 0, canvas.width, canvas.height);
							mediaViewerFactory.videoDragPath = scope.openm.files[scope.openm.fileIndex].path;
							electron.ipcRenderer.send('ondragstart', scope.openm.files[scope.openm.fileIndex].path, canvas.toDataURL());
						}
						
						mediaViewerFactory.videoPlayer.elements.controls.setAttribute("draggable", true);
						mediaViewerFactory.videoPlayer.elements.controls.ondragstart = (event) => 
						{				
							event.preventDefault();
							event.stopPropagation();
						}
						mediaViewerFactory.videoPlayer.elements.controls.onmouseup = (event) => 
						{				
							event.preventDefault();
							event.stopPropagation();
						}
						mediaViewerFactory.setZoom(scope.config.zoomFac);
						mediaViewerFactory.videoPlayer.elements.container.style.display = "block";
					},
					"leave": () => {
						if(mediaViewerFactory.videoPlayer.paused)
							return;
						mediaViewerFactory.videoPlayer.media.innerHTML = '';
						mediaViewerFactory.videoPlayer.stop();
						mediaViewerFactory.videoPlayer.elements.container.style.display = "none";
					}
				}
			};
			
			
			let debounced = (fn, delay) => {
				let timerId;
				return function (...args) {
				  clearTimeout(timerId);
				  timerId = setTimeout(() => {
					fn(...args);
					timerId = null;
				  }, delay);
				}
			}
			let resetIndex = false;
			let fileChangeCounter = 0;
			class MediaFolder
			{
				constructor()
				{
					this.reset();
					this.fileCounterReset  = debounced(() => fileChangeCounter = 0, 250);
					this.debouncedMediaSrc = debounced(() => this.setMediaDisplaySrc(), 250);
					this.displayTypes = ["image", "pdf", "video"];
				}
				reset()
				{
					if(this.files)
						this.revokeObjectURLs();
					this.loading = false;
					this.path = "";
					this.fileIndex = null;
					this.files = [];
					this.openFile = {};
				}

				setFilesLength() {domElems.filesLength.innerText = this.files.length};
				getSafeFileIndex(index) {return Math.min(Math.max(index,0),this.files.length-1);};
		
				setEndlessIndex() 
				{
					if(!resetIndex)
						return;
						
					for(let childElement of domElems.scrollContainer.children)
					{
						if(!childElement.complete)
							return;
							
						if(parseInt(childElement.getAttribute("fileIndex")) == this.fileIndex)
						{
							resetIndex=false;
							childElement.scrollIntoView();
							break;
						}
					}
				}
				setEndlessScrollFileIndex()
				{
					if(!scope.config.enableEndlessScroll)
						return;
					let elem = Array.from(domElems.scrollContainer.children).find( elem => elem.y>0 );
					this.fileIndex = elem ? parseInt(elem.getAttribute("fileIndex"))-1 :
						parseInt(domElems.scrollContainer.children[domElems.scrollContainer.children.length-1].getAttribute("fileIndex"));
					domElems.fileIndex.innerText = this.fileIndex+1;		
					this.loadZipImageData();
					mediaViewerFactory.onFileChange && mediaViewerFactory.onFileChange();
					scope.$digest();
				}

				setMediaDisplaySrc()
				{
					if(scope.config.enableEndlessScroll && !scope.$root.$$phase)
					{	
						scope.$digest();
						return;
					}
					if(!this.files[this.fileIndex])
						return;

					this.openFile = this.files[this.fileIndex];
					for(let displayType of this.displayTypes)
						if(this.openFile.mediaType != displayType)
							fileTypeAdapters[displayType].leave(this.openFile);
					fileTypeAdapters[this.openFile.mediaType].change(this.openFile);
					domElems.viewerContainer.scrollTop=0; 
					domElems.viewerContainer.scrollLeft=0;
					skrollTop.stop();
	
					for(let prefetch of domElems.imagePrefetch)
					{
						let prefetchFile = this.files[this.getSafeFileIndex(this.fileIndex + prefetch.offset)];
						if(prefetchFile && prefetchFile.src && !prefetchFile.zipFile)
						{
							prefetch.img.src = prefetchFile.src;
							// prefetch.decode && prefetch.img.decode().catch(console.log);
						}
					}
				}
				queueMediaDisplaySrc()
				{
					fileChangeCounter+=1;
					this.fileCounterReset();
					if(fileChangeCounter <= (scope.config.enableEndlessScroll ? 1 : 5))
						this.setMediaDisplaySrc();
					else
					{
						mediaViewerFactory.displayOverlay();
						this.debouncedMediaSrc();
					}
				}

				revokeObjectURLs(prefetchFrom = Infinity, prefetchTo = 0)
				{
					for(let i=0; i<this.files.length; i++)
					{
						if(this.files[i].zipFile && (i<prefetchFrom || i>prefetchTo))
						{
							if(this.files[i].mediaStream)
							{
								this.files[i].mediaStream.unpipe();
								this.files[i].mediaStream.destroy();
								this.files[i].mediaStream = null;
							}	
							URL.revokeObjectURL(this.files[i].src);
							this.files[i].src = null;
							this.files[i].loading = false;
						}
					}
				}
				async loadZipImageData()
				{
					let prefetchFactor = scope.config.enableEndlessScroll ? 9 : 1;
					let prefetchFrom = this.getSafeFileIndex(this.fileIndex-prefetchFactor);
					let prefetchTo = this.getSafeFileIndex(this.fileIndex+prefetchFactor);
					this.revokeObjectURLs(prefetchFrom, prefetchTo);
					
					for(let i=prefetchFrom; i<=prefetchTo; i++)
					{
						if(!this.files[i].src && !this.files[i].loading)
						{	
							this.files[i].loading = true;
							let bufs = [];
							this.files[i].mediaStream = await this.files[i].zipFile.openReadStream();
							this.files[i].mediaStream.on('data', (d) => {
								d._isBuffer = true;
								bufs.push(d);
							});
							this.files[i].mediaStream.on('error', (e) => debugLog(e) );
							this.files[i].mediaStream.on('end', () =>
							{
								this.files[i].src = URL.createObjectURL(new Blob([ Buffer.concat(bufs) ]));
								if(scope.config.enableEndlessScroll)
								{
									for(let i=prefetchFrom; i<=this.fileIndex; i++)
										if(!this.files[i].src)
											return;
									this.queueMediaDisplaySrc();
								}
								if(!scope.config.enableEndlessScroll && i==this.fileIndex)
									this.queueMediaDisplaySrc();
							});
						}
					}
				}

				setFileIndex(newIndex, forceLoad)
				{
					if(this.files.length == 0)
					{
						domElems.imageDisplay.style.display = "none";
						domElems.videoDisplay.style.display = "none";
						return;
					}

					let oldFile = this.files[this.fileIndex];
					this.fileIndex = this.getSafeFileIndex(newIndex);
					domElems.fileIndex.innerText = this.fileIndex+1;					
					mediaViewerFactory.onFileChange && mediaViewerFactory.onFileChange();
	
					if(scope.config.enableEndlessScroll)
					{	
						resetIndex = true;
						if(this.fileIndex<=9 || this.fileIndex>=this.files.length-9)
							this.setEndlessIndex();
					}
					if(this.files[this.fileIndex].zipFile)
						this.loadZipImageData();
					if(oldFile != this.files[this.fileIndex] || forceLoad)
						this.queueMediaDisplaySrc();
				}
			}


			//overlay
			let loadOverlay = () =>
			{
				for(let key in mediaViewerFactory.overlay)
				{
					if(mediaViewerFactory.overlay[key].length>0)
					{
						let element =  angular.element(domElems[key+"Overlay"]);
						element.append($compile(mediaViewerFactory.overlay[key].join(""))(scope));
					}
				}
				addDomElems(["fileIndex", "filesLength"]);
			}
			loadOverlay();

			scope.isFullScreen = electronWindow.isFullScreen();
			let enterFullScreen = () => { scope.isFullScreen = true;  scope.$digest(); };
			let leaveFullScreen = () => { scope.isFullScreen = false; scope.$digest(); };
			electronWindow.on('enter-full-screen', enterFullScreen);
			electronWindow.on('leave-full-screen', leaveFullScreen);
			window.addEventListener('beforeunload', () =>
			{
				electronWindow.removeListener('enter-full-screen', enterFullScreen);				
				electronWindow.removeListener('leave-full-screen', leaveFullScreen);
			});

			let overlaySizeMiddle = 194;
			let setOverlayPosition = entries =>
			{
				overlaySizeMiddle = domElems.centerOverlay.clientWidth || overlaySizeMiddle;
				let overlaySizeRight = domElems.rightOverlay.clientWidth;
				let rect = entries[0].boundingClientRect || entries[0].contentRect;
				if(rect.width < Math.max(336,(23 + overlaySizeMiddle + overlaySizeRight)))
					domElems.centerOverlay.style.display = "none";
				else
				{
					domElems.centerOverlay.style.right = domElems.rightOverlay.offsetLeft < 316 ? overlaySizeMiddle/2 + overlaySizeRight + "px" : "50%";
					domElems.centerOverlay.style.display = "flex";
					domElems.rightOverlay.style.display = "flex";
				}
			}
			(new ResizeObserver(setOverlayPosition)).observe(domElems.viewerOverlay);
			// (new IntersectionObserver(setOverlayPosition)).observe(domElems.viewerOverlay);


			let mouseState = {x:0, y:0};
			domElems.viewerContainer.parentNode.addEventListener("mousemove", (ev) =>
			{
				mouseState.x = ev.clientX;
				mouseState.y = ev.clientY;
			});
			
			domElems.viewerContainer.parentNode.addEventListener("mousemove", (ev) =>
			{
				if(scope.config.showOverlay != ev.clientY<100)
				{	
					scope.config.showOverlay = !scope.config.showOverlay;
					if(scope.config.hideSideMenu)
						scope.$digest();
				}
			});
			domElems.viewerContainer.parentNode.addEventListener("mouseleave", (ev) =>
			{
				mouseState.y = -1;
				if(scope.config.showOverlay)
				{	
					scope.config.showOverlay = false;
					if(scope.config.hideSideMenu)
						scope.$digest();
				}
			});

			let overlayLastTimeout;
			mediaViewerFactory.displayOverlay = () =>
			{
				scope.config.showOverlay = true;
				scope.$digest();
				clearTimeout(overlayLastTimeout);
				overlayLastTimeout = setTimeout(function()
				{
					if(scope.config.showOverlay != (mouseState.y<100 && mouseState.y>=0))
						scope.$apply( () => scope.config.showOverlay = (mouseState.y<100 && mouseState.y>=0) );
				}, 1000);
			}
			scope.toggleFullScreen = mediaViewerFactory.toggleFullScreen = () => {
				electronWindow.setFullScreen(!scope.isFullScreen);
			}
			scope.setZoom = mediaViewerFactory.setZoom = (zoomFactor) =>
			{
				let elems = document.getElementsByClassName("mediaDisplay");
				for(let i = 0; i<elems.length;i++)
					elems[i].style.zoom = parseFloat(zoomFactor);
				mediaViewerFactory.videoPlayer.media.style.zoom = parseFloat(zoomFactor);
			}
			mediaViewerFactory.changeZoom = (i) =>
			{
				scope.config.zoomFac = Math.max(scope.config.zoomFac + i, 0.00000001);
				mediaViewerFactory.setZoom(scope.config.zoomFac);
			}
			
			let mouseTimeout;
			document.addEventListener("mouseup"  , (ev) => window.clearTimeout(mouseTimeout));
			let uiMouseDown = (fct, mouseDownTimeFactor=0) =>
			{
				if(!mouseDownTimeFactor)
					fct();
				mouseTimeout = window.setTimeout(() =>
				{
					fct();
					uiMouseDown(fct, Math.min(1, mouseDownTimeFactor+0.1));
				}, 250*(1-mouseDownTimeFactor) + 25 );
			}
			scope.uiMouseDownZoom = (x) => uiMouseDown( () =>
			{
					mediaViewerFactory.changeZoom(x);
					!scope.$root.$$phase && scope.$digest();
			});
			scope.uiMouseDownFileIndex = (x) => uiMouseDown( () => scope.openm.setFileIndex(scope.openm.fileIndex+x) );



			//scrolling
			scope.$watch("config.dragscroll", () => scope.config.dragscroll ? domElems.viewerContainer.style.cursor = "grab" : domElems.viewerContainer.style.cursor = "default" );			
			scope.dragScrollStart = () => domElems.viewerContainer.style.cursor = "grabbing";
			scope.dragScrollEnd = (dragScope, ev, withinConstraints) =>
			{
				domElems.viewerContainer.style.cursor = "grab";
				scope.openm.setEndlessScrollFileIndex();
				if(withinConstraints)
					mouseUpHandler(ev);
			}


			scope.abs = Math.abs;
			scope.$watch("config.enableEndlessScroll", () => 
			{
				if(scope.openm && scope.config.enableEndlessScroll !== undefined)
					scope.openm.setFileIndex(scope.openm.fileIndex, true)
			});
			window.initEndlessScrollImg = (img) =>
			{
				img.style.zoom = parseFloat(scope.config.zoomFac);
				scope.openm.setEndlessIndex();
				img.decode();
			}


			let isSmoothScrolling = false;
			let handleSmoothScroll = (xOffset, yOffset) =>
			{
				if(isSmoothScrolling)
					return;
				isSmoothScrolling = true;
				skrollTop.scrollTo({
					element: domElems.viewerContainer,
					easing: (t, b, c, d) => b+c*t/8,
					scrollDirection: yOffset != 0 ? "scrollTop" : "scrollLeft",
					to:              yOffset != 0 ? domElems.viewerContainer.scrollTop+yOffset : domElems.viewerContainer.scrollLeft+xOffset,
					duration: 999999,
					callback: () => {
						isSmoothScrolling = false;
						scope.openm.setEndlessScrollFileIndex();
					}
				});
			}
			mediaViewerFactory.handleScroll = (xOffset,yOffset) =>
			{
				if(scope.config.enableSmoothScroll)
					handleSmoothScroll(xOffset,yOffset);
				else
				{
					domElems.viewerContainer.scrollLeft+=xOffset;
					domElems.viewerContainer.scrollTop+=yOffset;
				}
			}
			let stopScolling = () =>
			{
				skrollTop.stop();
				if(!scope.config.enableSmoothScroll)
					setEndlessScrollFileIndex();
			}



			//key/mouse handling
			mediaViewerFactory.getModifierString = () => Object.keys(keyMap).join("+");
			let execKeyFct = (key) => mediaViewerFactory.keyBindFcts[key.fct](key.x);
			let keyMap = {};
			let resetKeyMap = () => keyMap = {};
			window.addEventListener('blur', resetKeyMap);
			window.addEventListener('beforeunload', () => { window.removeEventListener('blur', resetKeyMap); });

			document.addEventListener("keydown", (ev) =>
			{
				keyMap[ev.code ? ev.code : ev.key] = true;
				if(ev.target.tagName == "INPUT" || ev.target.tagName == "TEXTAREA")
					return;

				let modifierString = mediaViewerFactory.getModifierString(ev);
				for(let key of scope.config.keyConfig)
				{
					if(modifierString == key.type)
					{	
						ev.preventDefault();
						execKeyFct(key);
						return;
					}
				}
			}, true);
			document.addEventListener("keyup", (ev) =>
			{
				let modifierString = mediaViewerFactory.getModifierString(ev);
				delete keyMap[ev.code ? ev.code : ev.key];
				for(let key of scope.config.keyConfig)
				{
					if(modifierString == key.type && (key.fct=="scrollVertical" || key.fct=="scrollHorizontal"))
					{
						stopScolling();
						return;
					}
				}
			}, true);		
			
			let mouseUpHandler = (ev) =>
			{
				if(ev.target.id=="videoDisplay" || ev.target.className=="vjs-modal-dialog-content"
				   || (ev.target.className.indexOf("vjs-")!=0 && ev.target.parentElement.className.indexOf("vjs-")!=0))
					execKeyFct(scope.config.mouseConfig[ev.button]);
			}

			domElems.viewerContainer.addEventListener("mouseup", (ev) => !scope.config.dragscroll && mouseUpHandler(ev) );
			domElems.viewerContainer.addEventListener("mousewheel", (ev) =>
			{
				ev.preventDefault();
				execKeyFct(scope.config.mouseConfig[ev.deltaY<0 ? 5 : 6]);
				clearTimeout(scope.scrollStopTimeout);
				scope.scrollStopTimeout = setTimeout(stopScolling, 100);
			});



			initMediaViewer();
		}
    };
}]);
	