angular.module("updateCheck", ['ngMaterial']).directive("updateCheckDialog",
["$rootScope", "$mdDialog",
( $rootScope ,  $mdDialog) =>
{
    return {
        restrict : 'E',
        templateUrl : 'js/directives/updateCheck.html',
		scope: {},
		link: async function(scope)
		{
			debugLog(scope);

			scope.config = $rootScope.$storage.config;
			scope.changeNotifyVersion = () => scope.config.notifyversion = scope.config.notifyversion == scope.updversion ? null : scope.updversion;

			let response = await fetch("https://copynippyjoin.gitlab.io/emdl-website/version",
			{ 
				method: 'GET',
				headers: { 'Cache-Control' : 'no-cache' }
			});
            scope.updversion = await response.text();
			scope.curversion = electronApp.getVersion();
			scope.showUpdateDialog = scope.curversion != scope.updversion && scope.config.notifyversion != scope.updversion;
			if(scope.showUpdateDialog)
			{
				$mdDialog.show(
				{
					contentElement: '#updateDialog',
					parent: angular.element(document.body),
					clickOutsideToClose: true,
					onComplete:function()
					{
						let updateWebview = document.getElementsByTagName("webview")[0];
						updateWebview.src = "https://copynippyjoin.gitlab.io/emdl-website/";
						// updateWebview.updgoback = updateWebview.goBack;
						// updateWebview.getWebContents().on('did-navigate', () => scope.$apply(() => scope.updshowback = updateWebview.canGoBack()));
						updateWebview.addEventListener("dom-ready", () =>
						{
							let updateWebContents = remote.webContents.fromId(updateWebview.getWebContentsId());
							updateWebContents.session.on('will-download', (event, item, webContents) =>
							{
								let totalBytes = item.getTotalBytes();
								item.on('updated', () => {
									if(!totalBytes)
										return;
									let progress = item.getReceivedBytes()/totalBytes;
									electronWindow.setProgressBar(progress);
								});
								item.once('done', () => electronWindow.setProgressBar(0) );
							});
						});
					}
				});	
			}
		}
    };
}]);
	