let axios = remote.require("axios");

angular.module("metaData", ['ngMaterial']).directive("metaDataDialog",
['ngUtil',	
( ngUtil) => 
{
    return {
		restrict : 'E',
        templateUrl : 'js/directives/metaData.html',
		scope: {},
		link: function(scope, element, attrs, controllers)
		{
			debugLog(scope);
			
			let replaceBr = (root) =>
			{
				let brElems = Array.from(root.getElementsByTagName("br"));
				brElems.forEach( (brElem) => brElem.replaceWith("\n") );	
			}
			let getElemsText = (root, tagName = "a") => Array.from(root.getElementsByTagName(tagName)).map( a => a.innerText);
			
			let getMuUrl = async (name) =>
			{
				let muURL;

				let baserequest = encodeURI("site:mangaupdates.com/series.html?id " + name.trim());
				let metaDataRequests = [
				{
					options: {
                        url: "https://www.mangaupdates.com/search.html",
						method: 'POST',
						headers: {'Content-Type':'application/x-www-form-urlencoded'},
						data: "search=" + encodeURI(name.trim())
					},
					callback: (dom) =>
					{
						let elem = dom.querySelector(".col-6.py-1.py-md-0.text a");
						if(elem && elem.href.includes("mangaupdates.com/series.html?id="))
							return elem.href;
					}
				},
				{
                    options: {
                        url: "https://duckduckgo.com/html/?q=" + baserequest
                    },	
					callback: (dom) =>
					{
						let elem = dom.getElementsByClassName("result__url");
						if(elem && elem.length>0)
							return elem[0].href;
					}
				},
				{
                    options: {
                        url: "https://www.startpage.com/do/search?q=" + baserequest,
                    },
					callback: (dom) =>
					{
						let elem = dom.querySelector(".w-gl__result-title");
						if(elem)
							return elem.href;
					}
				}];

                for(let metaDataRequest of metaDataRequests)
                {
					try
					{
                        let response = await axios(metaDataRequest.options);
						let dom = new DOMParser().parseFromString(response.data, "text/html");
						muURL = metaDataRequest.callback(dom);
						if(!muURL || !muURL.includes("mangaupdates"))
							throw "got mu URL " + muURL + " for " + metaDataRequest.options.url;
						else
							break;
					}
					catch(e) { console.warn(e); }
                }

				return muURL;
			}

			let extractMuMetaData = async (muURL) =>
			{
				let metaData = {};

                let response = await fetch(muURL);
                response = await response.text();
				let html = new DOMParser().parseFromString(response, "text/html");

				let sContainer = html.querySelectorAll("#main_content .row.no-gutters")[1];
				let sContent0 = sContainer.querySelectorAll(".col-6.p-2.text")[0].getElementsByClassName("sContent");
				let sContent1 = sContainer.querySelectorAll(".col-6.p-2.text")[1].getElementsByClassName("sContent");

				replaceBr(sContent0[0]);
				replaceBr(sContent0[2]);
				replaceBr(sContent0[3]);
				replaceBr(sContent0[5]);
				replaceBr(sContent0[6]);
				replaceBr(sContent0[8]);

				metaData.link = muURL;
				metaData.name = sContainer.getElementsByClassName("releasestitle")[0].innerText;

				let descElem = sContainer.querySelector("#div_desc_more") || sContent0[0];
				metaData.description = descElem.innerText.trim();
				metaData.type = sContent0[1].innerText.trim();
				metaData.relatedseries = sContent0[2].innerText.trim();
				metaData.asocnames = sContent0[3].innerText.trim();

				let latesttrans = sContent0[5].innerText.split("\n").filter(Boolean);
				metaData.latesttrans = latesttrans.splice(0,latesttrans.length-1).join("\n");
				metaData.latestrels = sContent0[6].innerText.trim();
				metaData.anstartend = sContent0[8].innerText.trim();
				let ratings = sContent0[11].innerText.split("\n");
				metaData.ratings = ratings[0] == "N/A" ? "N/A" : "Average:  " + ratings[0].split(" ")[1] + "\nBayesian: " + ratings[0].split(" ")[7];
				
				metaData.authors = getElemsText(sContent1[5]).join("\n");
				metaData.artists = getElemsText(sContent1[6]).join("\n");
				metaData.categories = getElemsText(sContent1[2], "li");
				let genre = getElemsText(sContent1[1]);
				metaData.genre = genre.slice(0,genre.length-1);
				metaData.year = sContent1[7].innerText;

				for(let property in metaData)
					if(metaData[property] == "")
						metaData[property] = "N/A";
				if(metaData.categories == "N/A")
					metaData.categories = ["N/A"];
				if(metaData.genre == "N/A")
					metaData.genre = ["N/A"];



				metaData.img = sContainer.getElementsByClassName("img-fluid")[0];
				if(metaData.img)
					metaData.img = metaData.img.src;
				return metaData;
			}


			let getMetaData = async function(name)
			{   
				let muURL = await getMuUrl(name);
				let metaData = {};
				if(!muURL)
				{
					metaData.error = true;
					console.error("couldn't fetch a mu URL");
				}
				else
				{
					try { metaData = await extractMuMetaData(muURL); }
					catch(e)
					{
						metaData.error = true;
						console.error("failed to extract mu MetaData for " + muURL, e);
					}
				}
				return metaData;
			}
			
			scope.ngUtil = ngUtil;
			ngUtil.showMetaInfo = async (name, ev) =>
			{
				scope.metaData = {};
				ngUtil.showDialog('#metaInfo', ev);
				scope.metaData = await getMetaData(name);
				scope.$digest();
			};

		}
    };
}]);
