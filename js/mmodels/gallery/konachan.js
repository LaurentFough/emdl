module.exports = 
{
	name:"konachan",
	options:
	{
		type:"gallery",
		livesearch: true,
		latency:0,
		custom:
		{
			comment:"Use imagetype 'highres' for a higher resolution or 'lowres' for a lower resolution.",
			imagetype:"lowres"
		}
	},

	getMlink: (m) => "https://konachan.com/post?tags=" + m.name + "&page=",

	cextract: (mmShared, m) =>
	{
		return mmShared.commonCextract(
		{
			requestCfg: { url: m.link+"1" },
			selectFct:  (dom) =>
			{
				let chapters = [];
				let lastChapterNumber = 1;
				let pageContainer = dom.getElementsByClassName("pagination")[0];
				if(pageContainer)
				{	
					let pageLinks = pageContainer.getElementsByTagName("a");
					lastChapterNumber = parseInt(pageLinks[pageLinks.length-2].innerText);
				}
				else
				{
					if(dom.getElementsByClassName("preview").length==0)
						return null;
				}

				for(let i=0; i<lastChapterNumber; i++)	
					chapters.push({volnumb:parseInt(i/100)+1, chanumb:(i+1), link:m.link+(i+1)});
				return chapters;
			}
		});
	},

	pextract: (mmSharedNode, ev) =>
	{
		return mmSharedNode.commonPextract(
		{
			requestCfg: { url: ev.data.curchap.link },
			selectFct:  ($) => $(".thumb"),
			elementFct: (elem,i) => { return {numb:i+1, link:"https://konachan.com"+elem.attr('href')}; }	
		});
	},

	iextract: (mmSharedNode, ev) =>
	{
		return mmSharedNode.commonIextract(
		{
			requestCfg: { url: ev.data.curpage.link },
			elementFct: ($) =>
			{
				let src = ev.data.options.custom.imagetype=="highres" ? $('#highres').attr('href') : $('#image').attr('src');
				return {src:src};
			}
		});
	}
};
