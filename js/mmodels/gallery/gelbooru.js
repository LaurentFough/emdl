module.exports = 
{
	name:"gelbooru",
	options:
	{
		type:"gallery",
		livesearch: true,
		latency:0,
		custom:
		{
			comment:"Use imagetype 'highres' for a higher resolution or 'lowres' for a lower resolution.",
			imagetype:"highres"
		}
	},

	getMlink: (m) => "https://gelbooru.com/index.php?page=post&s=list&tags="+m.name+"&pid=",

	cextract: (mmShared, m) =>
	{	
		return mmShared.commonCextract(
		{
			requestCfg: { url: m.link+"19992" },
			selectFct: (dom) =>
			{
				let chapters = [];
				let pageLinks = dom.getElementById("paginator").getElementsByTagName("a");
				if(pageLinks.length==0)
					return null;
				
				let lastChapterNumber = parseInt(pageLinks[pageLinks.length -1].getAttribute("href").split("=").slice(-1)[0])/42 + 1;
				for(let i=0; i<Math.min(477, lastChapterNumber); i++)	
					chapters.push({volnumb:parseInt(i/100)+1, chanumb:(i+1), link:m.link+(i*42)});
				return chapters;
			}
		});
	},

	pextract: (mmSharedNode, ev) =>
	{
		return mmSharedNode.commonPextract(
		{
			requestCfg: { url: ev.data.curchap.link },
			selectFct:  ($) => $("div.thumbnail-preview"),
			elementFct: (elem,i) => { return {numb:i+1, link:"https:"+elem.find("a").attr('href')}; }
		});
	},

	iextract: (mmSharedNode, ev) =>
	{
		return mmSharedNode.commonIextract(
		{
			requestCfg: { url:ev.data.curpage.link },
			elementFct: ($) =>
			{
				let src = ev.data.options.custom.imagetype=="highres" ? $("meta[property='og:image']").prop("content") : $('#image').attr('src');
				if(src[0]=="/" && src[1]=="/")
					src = "https:" + src;
				return {src:src};
			}
		});
	}
};
