module.exports = 
{
	name:"readcomicsonline",
	options:
	{
		type:"comic",
		latency:0
	},

	mextract: (mmShared) =>
	{
		return mmShared.commonMextract(
		{
			requestCfg: { url: "http://readcomicsonline.ru/changeMangaList?type=text" },
			selectFct:  (dom)  => dom.getElementsByTagName("a"),
			elementFct: (elem) =>
			{
				if(elem.href)
					return {"name":elem.getElementsByTagName("h6")[0].innerText, "link":elem.href};
				return null;
			}
		});
	},

	cextract: (mmShared, m) =>
	{	
		return mmShared.commonCextract(
		{
			requestCfg: { url: m.link },
			selectFct:  (dom)  =>
			{
				let selectElements = dom.getElementsByClassName("chapters")[0].getElementsByTagName("a");
				return [].slice.call(selectElements).reverse();
			},
			elementFct: (elem) =>
			{
				let chanumb = elem.innerText.split("#").filter(Boolean).splice(-1)[0];					
				return {"volnumb":1, "chanumb":chanumb, "link":elem.href};
			}
		});
	},

	pextract: (mmSharedNode, ev) =>
	{
		return mmSharedNode.commonPextract(
		{
			requestCfg: { url: ev.data.curchap.link },
			selectFct:  ($) => $("#page-list option"),
			elementFct: (elem,i) => { return {numb:i+1, link:ev.data.curchap.link+"/"+elem.attr('value')}; }	
		});
	},

	iextract: (mmSharedNode, ev) =>
	{
		return mmSharedNode.commonIextract(
		{
			requestCfg: { url: ev.data.curpage.link },
			elementFct: ($) => { return {src:$("#ppp img").attr("src")}; }
		});
	}
};
