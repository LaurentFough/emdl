module.exports = 
{
	name:"comicextra",
	options:
	{
		type:"comic",
		latency:0
	},

	mextract: (mmShared) =>
	{
		return mmShared.commonMextract(
		{
			requestCfg: { url: "http://www.comicextra.com/comic-list" },
			selectFct:  (dom)  => dom.querySelectorAll(".series-col a"),
			elementFct: (elem) =>
			{
				if(elem.href!="http://www.comicextra.com/")
					return { "name": elem.innerText, "link": elem.href };
				return null;
			}
		});
	},

	cextract: (mmShared, m) =>
	{	
		return mmShared.commonCextract(
		{
			requestCfg: { url: m.link },
			selectFct:  (dom)  =>
			{
				let selectElements = dom.getElementById("list").getElementsByTagName("a");
				return [].slice.call(selectElements).reverse();
			},
			elementFct: (elem) =>
			{
				let chanumb = /#((.)+)/g.exec(elem.innerText)[1];	
				return { "volnumb":1, "chanumb":chanumb, "link":elem.href };
			}
		});
	},

	pextract: (mmSharedNode, ev) =>
	{
		return mmSharedNode.commonPextract(
		{
			requestCfg: { url:ev.data.curchap.link+"/full", headers:{"User-Agent": "Mozilla/5.0"} },
			selectFct:  ($) => $(".chapter-container img"),
			elementFct: (elem,i) => { return {numb:i+1, src:elem.attr('src')}; }	
		});
	}
};
