module.exports = 
{
	name:"mangatown",
	options:
	{
		type:"manga",
		latency:400
	},

	mextract: (mmShared) =>
	{
		return mmShared.commonMextract(
		{
			requestCfg: { url: (i) => "https://www.mangatown.com/directory/0-0-0-0-0-0/"+(i+1)+".htm?name.az" },
			options:
			{
				maxIterations: 1500,
				setMaxIterations: (dom) => dom.getElementsByClassName("next-page")[0].getElementsByTagName("select")[0][0].innerText.replace("1/","")
			},
			selectFct:  (dom)  => dom.getElementsByClassName("manga_cover"),
			elementFct: (elem) => { return {name:elem.title, link:"https://www.mangatown.com"+elem.getAttribute('href')}; },
		});
	},

	cextract: (mmShared, m) =>
	{	
		return mmShared.commonCextract(
		{
			requestCfg: { url: m.link },
			selectFct:  (dom) =>
			{
				let selectElements = dom.getElementsByClassName("chapter_list")[0].getElementsByTagName("a");
				return [].slice.call(selectElements).reverse();
			},
			elementFct: function(elem)
			{
				let link = elem.getAttribute('href');
				if(!elem.href)
					return null;
				let chanumb = /c(\d+[\.?\d*]*)\//g.exec(link)[1]; 
				let volregex = /(Vol\ )(\d+(\.?\d*)*)/g.exec(link); 
				let volnumb = volregex ? volregex[2] : null;		
				return {"volnumb":volnumb, "chanumb":chanumb, "link":"https://www.mangatown.com"+link};
			}
		});
	},

	pextract: (mmSharedNode, ev) =>
	{
		let urlTemplate = ev.data.curchap.link;
		return mmSharedNode.commonPextract(
		{
			requestCfg: { url:ev.data.curchap.link },
			selectFct:  ($) => $(".manga_read_footer .page_select select option"),
			elementFct: (elem,i) =>
			{
				let numb = parseFloat(elem.text());
				let link = i==0 ? urlTemplate : urlTemplate+numb+".html";
				if(numb)
					return {numb:numb, link:link};
				return null;
			}
		});
	},

	iextract: (mmSharedNode, ev) =>
	{
		return mmSharedNode.commonIextract(
		{
			requestCfg: { url: ev.data.curpage.link },
			elementFct: ($) => { return {src: "https:"+$('#image').attr('src')}; }
		});
	}
};
