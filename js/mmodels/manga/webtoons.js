module.exports = 
{
	name:"webtoons.com",
	options:
	{
		type:"manga",
		latency:0
	},

	mextract: (mmShared) =>
	{
		return mmShared.commonMextract(
		{
			requestCfg:{ url: (i) => "https://www.webtoons.com/en/challenge/list?genreTab=ALL&sortOrder=LIKEIT&page="+(i+1), headers: { 'cookie': "ageGatePass=true;" } },
			options:
			{
				maxIterations: 500,
				latency: 500,
				cancelOnEmpty: true,
				requestLibrary: "axios"
			},
			selectFct: (dom)  => 
			{
				if(dom.getElementsByClassName("paginate")[0].getElementsByTagName("a").length == 0)
					return null;
				return dom.getElementsByClassName("challenge_lst")[0].getElementsByClassName("challenge_item");
			},
			elementFct: (elem) => { return {name:elem.getElementsByClassName("subj")[0].innerText.trim(), link:elem.href}; },
		});
	},

	cextract: async (mmShared, m) =>
	{
		let axios = remote.require("axios");
		let response = await axios({ url: m.link, headers: { 'cookie': "ageGatePass=true;" }});
		let requestLink = new DOMParser().parseFromString(response.data, "text/html").getElementById("_btnEpisode").href;
		return mmShared.commonCextract(
		{
			requestCfg:{ url: requestLink, headers: { 'cookie': "ageGatePass=true;" }},			
			options:{ requestLibrary: "axios" },
			selectFct:  (dom) => dom.getElementsByClassName("episode_cont")[0].getElementsByTagName("li"),
			elementFct: (elem) => { return {"volnumb":null, "chanumb":null , "link":elem.getElementsByTagName("a")[0].href}; }
		});
	},

	pextract: (mmSharedNode, ev) =>
	{
		return mmSharedNode.commonPextract(
		{
			requestCfg: { url: ev.data.curchap.link },
			selectFct:  ($) => $("#_imageList img"),
			elementFct: (elem,i) =>
			{
				let page =
				{
					numb: i+1,
					src:  elem.attr('data-url'),
					options:
					{
						requestCfg:
						{
							headers:
							{
								"User-Agent": "Mozilla/5.0",
								"Host": "webtoon-phinf.pstatic.net",
								"Referer": "https://www.webtoons.com/"
							}
						}
					}
				}
				return page;
			}
		});
	}
};
