module.exports = 
{
	name:"mangahere",
	options:
	{
		type:"manga",
		latency:400
	},

	mextract: (mmShared) =>
	{
		return mmShared.commonMextract(
		{
			requestCfg: { url: "https://www.mangahere.cc/mangalist/" },
			selectFct:  (dom)  => dom.getElementsByClassName("browse-new-block-content"),
			elementFct: (elem) =>
			{
				let mediaElement = elem.getElementsByTagName("a")[0];
				return {"name":mediaElement.innerText, "link":"https://www.mangahere.cc"+mediaElement.getAttribute('href')};
			},
		});
	},

	cextract: (mmShared, m) =>
	{	
		return mmShared.commonCextract(
		{
			requestCfg: { url: m.link, headers: {Cookie: "isAdult=1"} },
			options: { requestLibrary: "axios" },
			selectFct: (dom) =>
			{
				let selectElements = dom.getElementsByClassName("detail-main-list")[0].getElementsByTagName("a");
				return [].slice.call(selectElements).reverse();
			},
			elementFct: (elem) =>
			{
				let link = "https://www.mangahere.cc"+elem.getAttribute('href');
				let volnumb = mmShared.applyVolNumbRegex(elem.innerText);
				let chanumb = mmShared.applyChaNumbRegex(elem.innerText);	
				return {"volnumb":volnumb, "chanumb":chanumb, "link":link};
			}
		});
	},

	pextract: (mmSharedNode, ev) =>
	{
		let urlTemplate = ev.data.curchap.link.replace("//www.mangahere.cc/","//m.mangahere.cc/").replace("1.html","");
		return mmSharedNode.commonPextract(
		{
			requestCfg: { url:urlTemplate },
			selectFct:  ($) => $(".mangaread-page").eq(0).find("option"),
			elementFct: (elem) =>
			{
				let numb = parseFloat(elem.text());
				if(numb)
					return {numb:numb, link:urlTemplate+numb+".html"};
				return null;
			}
		});
	},

	iextract: (mmSharedNode, ev) =>
	{
		return mmSharedNode.commonIextract(
		{
			requestCfg: { url: ev.data.curpage.link },
			elementFct: ($) => { return {src:$('#image').attr('src')}; }
		});
	}
};
