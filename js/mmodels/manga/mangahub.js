module.exports = 
{
	name:"mangahub",
	options:
	{
		type:"manga",
		latency:0
	},

	mextract: (mmShared) =>
	{
		return mmShared.commonMextract(
		{
			requestCfg: { url: (i) => "https://mangahub.io/search/page/"+(i+1)+"?q=&order=ALPHABET&genre=all" },
			options:
			{	
				maxIterations: 1500,
				cancelOnEmpty: true
			},
			selectFct:  (dom) => dom.getElementsByClassName("media-manga"),
			elementFct: (elem) =>
			{
				let mediaElement = elem.getElementsByClassName("media-heading")[0].getElementsByTagName("a")[0];
				return {name:mediaElement.innerText, link:mediaElement.href};
			},
		});
	},

	cextract: (mmShared, m) =>
	{	
		return mmShared.commonCextract(
		{
			requestCfg: { url: m.link },
			selectFct:  (dom) =>
			{
				let chapters = [];
				let chapterLists = dom.getElementsByClassName("list-group"); 
				for(let chapterList of chapterLists)
					chapters = chapters.concat([].slice.call(chapterList.getElementsByTagName("a")));
				return chapters.reverse();
			},
			elementFct: (elem) =>
			{
				let chanumb = elem.innerText.split("#")[1].split(" ")[0]; 
				let volregex = /([vV]ol\.\ ?)(\d+(\.?\d*)*)/g.exec(elem.innerText); 
				let volnumb = volregex ? volregex[2] : undefined;			
				return {"volnumb":volnumb, "chanumb":chanumb, "link":elem.href};
			}
		});
	},

	pextract: (mmSharedNode, ev) =>
	{
		return mmSharedNode.commonPextract(
		{
			requestCfg: { url: ev.data.curchap.link },
			selectFct: ($) =>
			{
				let chapters = [];
				let firstImage = $("#mangareader img").eq(0);
				let imgBase = firstImage.attr("src").replace("/1.jpg","/");
				let maxPage = parseFloat(firstImage.next().text().split("/")[1]) || 100;
				for(let i=1; i<=maxPage; i++)
					chapters.push({numb:i, src:imgBase+i+".jpg"});
				return chapters;
			}	
		});
	}
};
