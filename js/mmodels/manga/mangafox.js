module.exports = 
{
	name:"mangafox",
	options:
	{
		type:"manga",
		latency:400,
		concurrencyLevel:"afterMediaSave",
		removeWatermarks:true
	},

	getWatermarkConfig: (image, nodeUtil) => 
	{
		let hashConfig = 
		[
			{
				area:{x:image.bitmap.width/2-344, y:image.bitmap.height-85, w:688, h:85},
				hash:["8o0x0200000", "a40w0100000", "ak0w0000000"]
			},
			{
				area:{x:image.bitmap.width/2-305, y:image.bitmap.height-65, w:610, h:65},
				hash:["8E000000000", "8E0w0200000"]
			},
			{
				area:{x:image.bitmap.width/2-305, y:image.bitmap.height-45, w:610, h:45},
				hash:["aw000000000"]
			},
			{
				area:{x:image.bitmap.width/2-305, y:image.bitmap.height-31, w:610, h:31},
				hash:["aM0A0200800"]
			}
		];
		return nodeUtil.getHashWatermarkConfig(hashConfig);
	},

	mextract: (mmShared) =>
	{
		return mmShared.commonMextract(
		{
			requestCfg: { url: (i) => "https://fanfox.net/directory/"+(i+1)+".html?az" },
			options:
			{
				maxIterations: 1500,
				setMaxIterations: (dom) => 
				{
					let selectElements = dom.getElementsByClassName("pager-list-left")[0].getElementsByTagName("a");
					return selectElements[selectElements.length-2].innerText;
				}
			},
			selectFct:  (dom)  => dom.getElementsByClassName("manga-list-1-list")[0].getElementsByTagName("li"),
			elementFct: (elem) =>
			{
				let mediaElements = elem.getElementsByTagName("a");
				let name = mediaElements[1].title || mediaElements[1].innerText;
				return {name:name, link:"https://fanfox.net"+mediaElements[0].getAttribute('href')};
			},
		});
	},

	cextract: (mmShared, m) =>
	{	
		return mmShared.commonCextract(
		{
			requestCfg: { url: m.link.replace("://fanfox.net/","://m.fanfox.net/") },
			selectFct:  (dom) => Array.from(dom.querySelectorAll(".chlist a")).reverse(),
			elementFct: (elem) =>
			{
				let link = "https:"+elem.getAttribute('href');
				let volnumb = mmShared.applyVolNumbRegex(elem.parentNode.parentNode.getElementsByTagName("dt")[0].innerText);
				let chanumb = mmShared.applyChaNumbRegex(elem.innerText);
				return {"volnumb":volnumb, "chanumb":chanumb, "link":link};
			}
		});
	},

	pextract: (mmSharedNode, ev) =>
	{
		let urlTemplate = ev.data.curchap.link.match(/^((?!\d+\.html).)*/)[0];
		return mmSharedNode.commonPextract(
		{
			requestCfg: { url:ev.data.curchap.link },
			selectFct:  ($) => $("select.mangaread-page option"),
			elementFct: (elem) =>
			{
				let numb = parseFloat(elem.text());
				if(numb)
					return {numb:numb,link:urlTemplate+numb+".html"};
				return null;
			}
		});
	},

	iextract: (mmSharedNode, ev) =>
	{
		return mmSharedNode.commonIextract(
		{
			requestCfg: { url: ev.data.curpage.link },
			elementFct: ($) => { return {src:$('#image').attr("src")}; }
		});
	}
};
