module.exports = 
{
	name:"mangaseeonline",
	options:
	{
		type:"manga",
		latency:0
	},

	mextract: (mmShared) =>
	{
		return mmShared.commonMextract(
		{
			requestCfg: { url: "https://mangaseeonline.us/directory/" },
			selectFct:  (dom)  => dom.getElementsByClassName("ttip"),
			elementFct: (elem) => { return {"name":elem.innerText, link:"https://mangaseeonline.us"+elem.getAttribute('href')} }
		});
	},

	cextract: (mmShared, m) =>
	{	
		return mmShared.commonCextract(
		{
			requestCfg: {url: m.link},
			selectFct:  (dom) =>
			{
				let selectElements = dom.getElementsByClassName("list chapter-list")[0].getElementsByTagName("a");
				return [].slice.call(selectElements).reverse();
			},
			elementFct: (elem) =>
			{
				let chanumb = elem.outerHTML.match(/chapter=\".*?\"/)[0].replace("chapter=\"","").replace("\"","");
				return {"volnumb":1, "chanumb":chanumb, "link":"https://mangaseeonline.us"+elem.getAttribute('href')};
			}
		});
	},

	pextract: (mmSharedNode, ev) =>
	{
		let urlTemplate = ev.data.curchap.link.match(/^((?!\d+\.html).)*/)[0];
		return mmSharedNode.commonPextract(
		{
			requestCfg: { url: ev.data.curchap.link },
			selectFct:  ($) => $('.PageSelect.hidden-xs option'),
			elementFct: (elem) =>
			{
				let numb = elem.attr('value');
				if(numb)
					return {numb:numb, link:urlTemplate+numb+".html"};
				return null;
			}
		});
	},

	iextract: (mmSharedNode, ev) =>
	{
		return mmSharedNode.commonIextract(
		{
			requestCfg: { url: ev.data.curpage.link },
			elementFct: ($) =>{ return {src:$('.CurImage').attr('src')}; }
		});
	}
};
