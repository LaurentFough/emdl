module.exports = 
{
	name:"mangapark",
	options:
	{
		type:"manga",
		latency:0
	},

	mextract: (mmShared) =>
	{
		return mmShared.commonMextract(
		{
			requestCfg: { url: (i) => "https://mangapark.net/search?orderby=a-z&page="+(i+1), headers:{Cookies: "h=1"} },
			options:
			{
				
				maxIterations: 1500,
				setMaxIterations: (dom) => dom.querySelector(".pager select option").innerText.split(" / ")[1]
			},
			selectFct:  (dom) => dom.querySelectorAll(".manga-list .item .cover"),
			elementFct: (elem) =>
			{
				return {name:elem.title, link:"https://mangapark.net"+elem.getAttribute('href')};
			},
		});
	},

	cextract: (mmShared, m) =>
	{	
		return mmShared.commonCextract(
		{
			requestCfg: { url: m.link, headers:{Cookies: "h=1"} },
			selectFct:  (dom)  => dom.querySelectorAll("#list .stream"),
			elementFct: (elem) =>
			{
				let chapters = [];
				let versionName = elem.querySelector("div div a").innerText;
				let chapterArray = Array.from(elem.querySelectorAll(".volume .chapter .ch")).reverse();
				for(let chapterElem of chapterArray)
				{	
					let volnumb = mmShared.applyVolNumbRegex(chapterElem.innerText);
                    let chanumb = mmShared.applyChaNumbRegex(chapterElem.innerText);
					chapters.push({volnumb:volnumb, chanumb:chanumb, version:versionName, link:"https://mangapark.net" + chapterElem.getAttribute('href')});
				}	
				return chapters;
			},
		});
	},

	pextract: (mmSharedNode, ev) =>
	{
        if(ev.data.curchap.link.slice(-2) == "/1")
            ev.data.curchap.link = ev.data.curchap.link.substring(0, ev.data.curchap.link.length - 2);
		return mmSharedNode.commonPextract(
		{
			requestCfg: { url: ev.data.curchap.link, headers:{Cookies: "h=1"} },
			selectFct: ($) =>
			{
				let pagesArray = [];
				let imgArray;
				try
				{
					imgArray = JSON.parse($.html().split("var images = ")[1].split(";")[0]);
				}
				catch(e)
				{
					imgArray = JSON.parse($.html().split("var _load_pages = ")[1].split(";")[0]);
					imgArray = imgArray.map(elem => elem.u);
                }

				for(let i=0; i<imgArray.length; i++)
				{
                    let imgURL = imgArray[i][0]=="/" && imgArray[i][1]=="/" ? "https:" + imgArray[i] : imgArray[i];
                    pagesArray.push( {numb:i+1, src: imgURL} );

                }	
				return pagesArray;
			}
		});
	}
};
