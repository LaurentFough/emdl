module.exports = 
{
	name:"mangadex",
	options:
	{
		type:"manga",
		latency:500,
		concurrencyLevel:"afterMediaSave"
	},

	mextract: (mmShared) =>
	{
		return mmShared.commonMextract(
		{
			requestCfg: { url: (i) => "https://mangadex.org/titles/0/"+(i+1)+"/?s=2#listing" },
			options:
			{
				maxIterations: 1500,
				setMaxIterations: (dom) => {
					let pageElements =  dom.querySelectorAll(".pagination .page-link");
					return pageElements[pageElements.length - 1].getAttribute("href").split("/").filter(Boolean).slice(-1)[0];
				}
			},
			selectFct:  (dom)  => dom.querySelectorAll("a.manga_title"),
			elementFct: (elem) => { return {name:elem.innerText, link:"https://mangadex.org"+elem.getAttribute("href")}; },
		});
	},

	cextract: async (mmShared, m) =>
	{	
		let apiLink = "https://mangadex.org/api/?id="+m.link.split("title/")[1].split("/")[0]+"&type=manga";
		let axios = require("axios");
		let response = await axios(apiLink);
		let chapters = [];
		for(let key in response.data.chapter)
		{
			let chapter = response.data.chapter[key];
			let link = "https://mangadex.org/api/?id="+key+"&server=null&type=chapter";
			chapter = {"version":chapter.lang_code + " - " + chapter.group_name, "volnumb":parseInt(chapter.volume), "chanumb":chapter.chapter, "link":link};
			chapters.push(chapter);
		}
		return chapters;
	},

	pextract: async (mmSharedNode, ev) =>
	{
		let axios = require("axios");
		let response = await axios(ev.data.curchap.link);
		let pages = [];
		for(let i=0; i<response.data.page_array.length; i++)
			pages.push({src:response.data.server+"/"+response.data.hash+"/"+response.data.page_array[i]});
		return pages;
	}
};
