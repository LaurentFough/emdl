module.exports = 
{
	name:"manganel",
	options:
	{
		type:"manga",
		latency:0
	},

	mextract: (mmShared) =>
	{
		return mmShared.commonMextract(
		{
			requestCfg: { url: (i) => "https://manganelo.com/advanced_search?s=all&orby=az&page="+(i+1) },
			options:
			{
				maxIterations: 1500,
				setMaxIterations: (dom) => dom.getElementsByClassName("page-last")[0].href.split("=").slice(-1)[0]
			},
			selectFct:  (dom)  => dom.getElementsByClassName("content-genres-item"),
			elementFct: (elem) =>
			{
				let mediaElement = elem.getElementsByTagName("h3")[0].getElementsByTagName("a")[0];
				return {name:mediaElement.innerText, link:mediaElement.href};
			},
		});
	},

	cextract: (mmShared, m) =>
	{	
		return mmShared.commonCextract(
		{
			requestCfg: {url:m.link},
			selectFct:  (dom)  =>
			{
				let selectElements = dom.getElementsByClassName("row-content-chapter")[0].getElementsByTagName("a");
				return [].slice.call(selectElements).reverse();
			},
			elementFct: (elem) =>
			{
				let volnumb = mmShared.applyVolNumbRegex(elem.innerText);
				let chanumb = mmShared.applyChaNumbRegex(elem.innerText);		
				return {"volnumb":volnumb, "chanumb":chanumb, "link":elem.href};
			}
		});
	},

	pextract: (mmSharedNode, ev) =>
	{
		return mmSharedNode.commonPextract(
		{
			requestCfg: { url:ev.data.curchap.link },
			selectFct:  ($) => $(".container-chapter-reader img"),
			elementFct: (elem,i) => { return {numb:i+1, src:elem.attr('src'), options: { options: { requestLibrary: "hooman" } }}; }	
		});
	}
};
