module.exports = 
{
	name:"readmanga",
	options:
	{
		type:"manga",
		latency:0
	},

	mextract: (mmShared) =>
	{
		return mmShared.commonMextract(
		{
			requestCfg: { url: (i) => "https://www.readmng.com/latest-releases/"+(i+1) },
			options:
			{
				requestLibrary: "hooman",
				dedupe: true,
				maxIterations: 1500,
				setMaxIterations: (dom) =>
				{
					let pageLinks = dom.getElementsByClassName("pagination")[0].getElementsByTagName("a");
					return (parseInt(pageLinks[pageLinks.length-1].href.split("/").slice(-1)[0])-1)
				}				
			},
			selectFct:  (dom)  => dom.getElementsByClassName("manga_updates")[0].getElementsByTagName("dt"),
			elementFct: (elem) =>
			{
				let mediaElement = elem.getElementsByTagName("a")[0];
				return {"name":mediaElement.innerText, "link":mediaElement.href};
			},
		});
	},

	cextract: async (mmShared, m) =>
	{
		return mmShared.commonCextract(
		{
			requestCfg: { url: m.link },
			options: { requestLibrary: "hooman" },
			selectFct: (dom)  =>
			{
				let selectElements = dom.getElementsByClassName("chp_lst")[0].getElementsByTagName("a");
				return [].slice.call(selectElements).reverse();
			},
			elementFct: (elem) =>
			{
				let chanumb = elem.href.split("/").filter(Boolean).splice(-1)[0];					
				return {"volnumb":1, "chanumb":chanumb, "link":elem.href};
			}
		});
	},

	pextract: (mmSharedNode, ev) =>
	{
		return mmSharedNode.commonPextract(
		{
			requestCfg:{ url: ev.data.curchap.link+"/all-pages" },
			options: { requestLibrary: "hooman" },
			selectFct:  ($) => $(".img-responsive"),
			elementFct: (elem,i) =>
			{
				let page =
				{
					numb: i+1,
					src:  elem.attr("src"),
					options: { options: { requestLibrary: "hooman" } }
				}
				return page;
			}
		});
	}
};
