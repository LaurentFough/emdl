module.exports = 
{
	name:"kissmanga",
	options:
	{
		type:"manga",
		latency:0
	},

	mextract: (mmShared) =>
	{
		return mmShared.commonMextract(
		{
			requestCfg: { url: (i) => "https://kissmanga.com/MangaList?page="+(i+1) },
			options:
			{
				requestLibrary: "hooman",
				maxIterations: 1000,
				setMaxIterations: (dom) =>
				{
					let pageLinks = dom.getElementsByClassName("pager")[0].getElementsByTagName("a");
					return pageLinks[pageLinks.length-1].getAttribute("page");
				}				
			},
			selectFct:  (dom)  => Array.from(dom.querySelectorAll(".listing tr")).slice(2),
			elementFct: (elem) =>
			{
				let mediaElement = elem.getElementsByTagName("a")[0];
				return {"name":mediaElement.innerText, "link":"https://kissmanga.com" + mediaElement.getAttribute('href')};
			},
		});
	},

	cextract: async (mmShared, m) =>
	{
		return mmShared.commonCextract(
		{
			requestCfg: { url: m.link },
			options: { requestLibrary: "hooman" },
			selectFct: (dom)   => Array.from(dom.querySelectorAll(".listing tr")).slice(2).reverse(),
			elementFct: (elem) =>
			{
				let chapterElem = elem.getElementsByTagName("a")[0];
				let volnumb = mmShared.applyVolNumbRegex(chapterElem.innerText);
				let chanumb = mmShared.applyChaNumbRegex(chapterElem.innerText);
				return {"volnumb":volnumb, "chanumb":chanumb, "link":"https://kissmanga.com"+chapterElem.getAttribute('href')};
			}
		});
	},

	pextract: (mmSharedNode, ev) =>
	{
		return mmSharedNode.commonPextract(
		{
			requestCfg:{ url: ev.data.curchap.link+"/all-pages" },
			options: { requestLibrary: "hooman" },
			selectFct:  async ($) =>
			{
				let encodedLinks = $.html().split("var lstImages = new Array();")[1].split("lstImages.push(wrapKA(\"").map( str => str.trim().split("\"));")[0] ).filter(Boolean);

				let decodeEscapeSequence = (str) => str.replace(/\\x([0-9A-Fa-f]{2})/g, (a,b) => String.fromCharCode(parseInt(b, 16)) );
				let hooman = require("hooman");
				let cryptokeys = await hooman("https://kissmanga.com/Scripts/lo.js");
				cryptokeys = decodeEscapeSequence(cryptokeys).split("\"HmacSHA256\",")[1].split(",\"parse")[0].replace(/\"/g, "").split(",");

				let firstKey;
				try{
					firstKey = decodeEscapeSequence($.html().split("_0xa5a2 = [\"")[1].split("\"];")[0]);
				} catch(e){
					console.log(e);
				}

				function wrapKA(inputString)
				{	
					let nodeCrypto = require('crypto');
					let algorithm = 'aes-256-cbc';
					let iv  = cryptokeys[0];
					let key = firstKey ? firstKey : cryptokeys[1];
					iv  = Buffer.from(iv, "hex");
					key = nodeCrypto.createHash('sha256').update(key).digest();

					let linkDecipheriv = nodeCrypto.createDecipheriv(algorithm, key, iv);
					let decodedString = linkDecipheriv.update(inputString, 'base64', 'utf-8');
					decodedString += linkDecipheriv.final();
					return decodedString;
				}

				let pages = [];
				for(let encodedLink of encodedLinks)
					pages.push({src:wrapKA(encodedLink)});
				return pages;
			}
		});
	}
};
