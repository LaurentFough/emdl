"use strict";

const nodeRequire = require;
const electron = require('electron');
const remote = electron.remote;
const electronApp = remote.app;
const isPackaged = electronApp.isPackaged;
const debugLog = (...args) => 
{
    if(!isPackaged)
        console.log(...args);
}
const electronWindow = remote.getCurrentWindow();
if(!isPackaged)
    electronWindow.webContents.openDevTools();


const path = require('path');

const appPath = electronApp.getAppPath();
const jsBase = path.join(appPath, "js");
const downloadsPath = electronApp.getPath("downloads").replace(/\\/g,"/");
const userDataPath = electronApp.getPath("userData").replace(/\\/g,"/");
const userPaths = 
{
	"downloads":      downloadsPath,
	"userData":       userDataPath,
	"mlists":         path.join(userDataPath, "mlists"),
	"themes":         path.join(userDataPath, "themes"),
	"preConfigFile":  path.join(userDataPath, "preConfig.json"),
	"configFile":     path.join(userDataPath, "config.json"),
	"jsBase":         jsBase,
	"mmodels":        path.join(jsBase, "mmodels"),
	"remoteMmodels":  path.join(jsBase, "remoteMmodels")
}
const userThemes = {};

const renderModule = require("module");
renderModule.globalPaths.push(appPath);


const loadCSS = (filePath) =>
{
    let elem = document.createElement("link");
    elem.setAttribute("rel", "stylesheet");
    elem.setAttribute("type", "text/css");
    elem.setAttribute("href", filePath);
    document.getElementsByTagName("head")[0].prepend(elem);
};
const loadJS = function(url)
{
    return new Promise( (resolve, reject) =>
    {

        let scriptTag = document.createElement('script');
        scriptTag.src = url;
        scriptTag.onload = resolve;
        document.head.appendChild(scriptTag);
    })
};

let startApplication = async () =>
{
    if(isPackaged)
    {
        await loadJS('./build/frontEnd.bundle.js');
        await loadJS('./build/app.bundle.js');
    }
    else
    {
        await loadJS('./js/frontEnd.entry.js');
        await loadJS('./js/app.entry.js');
    }
}

startApplication();
