"use strict";

let testCases = {};

testCases["comicextra"] =
{
	"cases":
	[
		{
			mextract: {"output":{"name":"1985: Black Hole Repo", "link":"https://www.comicextra.com/comic/1985-black-hole-repo"}},
			cextract: {"input":{"link":"https://www.comicextra.com/comic/1985-black-hole-repo"},
					   "chapters":[{volnumb: 1, chanumb: "1", link: "https://www.comicextra.com/1985-black-hole-repo/chapter-1"}]},
			pextract: {"input":{"link":"https://www.comicextra.com/1985-black-hole-repo/chapter-1"},
					   "pages":[{numb: "0001", src: "https://2.bp.blogspot.com/-lZeCQMiuDVU/WfqLd_iHCeI/AAAAAAAAK8g/BXu2mUFdTmYCU0Ra4umjEViVnVwsUuESACHMYCw/s0/RCO001_w.jpg"}]}
		}
	]
};

testCases["gelbooru"] =
{
	"cases":
	[
		{
			mextract: {"output":{"name":"wallpaper no_humans castle", "link":"https://gelbooru.com/index.php?page=post&s=list&tags=wallpaper+no_humans+castle&pid="}},
			cextract: {"input":{"link":"https://gelbooru.com/index.php?page=post&s=list&tags=wallpaper+no_humans+castle&pid="},
					   "chapters":[{volnumb: 1, chanumb: 1, link: "https://gelbooru.com/index.php?page=post&s=list&tags=wallpaper+no_humans+castle&pid=0"}]},
			pextract: {"input":{"link":"https://gelbooru.com/index.php?page=post&s=list&tags=wallpaper+no_humans+castle&pid=0"},
					   "pages":[{"numb":"0001","link":"https://gelbooru.com/index.php?page=post&s=view&id=3970294&tags=wallpaper no_humans castle"}]},
		    iextract: {"input":{"link":"https://gelbooru.com/index.php?page=post&s=view&id=3970294"}}
		}
	]
};

testCases["kissmanga"] =
{
	"cases":
	[
		{
			mextract: {"output":{"name":"Blame!", "link":"https://kissmanga.com/Manga/Blame"}},
			cextract: {"input":{"link":"https://kissmanga.com/Manga/Blame"},
					   "chapters":[{volnumb: "001", chanumb: "001", link: "https://kissmanga.com/Manga/Blame/Vol-001-Ch-001-Read-Online?id=97301"}]},
			pextract: {"input":{"link":"https://kissmanga.com/Manga/Blame/Vol-001-Ch-001-Read-Online?id=97301"},
					   "pages":[{src: "http://images-focus-opensocial.googleusercontent.com/gadgets/proxy?container=focus&gadget=a&no_expand=1&resize_h=0&rewriteMime=image%2F*&url=http%3a%2f%2fi.imgur.com%2frXw6S.jpg&imgmax=25000", numb: "0001"}]}
		}
	]
};

testCases["konachan"] =
{
	"cases":
	[
		{
			mextract: {"output":{"name":"blame monochrome", "link":"https://konachan.com/post?tags=blame+monochrome&page="}},
			cextract: {"input":{"link":"https://konachan.com/post?tags=blame+monochrome&page="},
					   "chapters":[{"volnumb":"0001","chanumb":"0001","link":"https://konachan.com/post?tags=blame+monochrome&page=1"}]},
			pextract: {"input":{"link":"https://konachan.com/post?tags=blame+monochrome&page=1"},
					   "pages":[{"numb":"0001","link":"https://konachan.com/post/show/72141/blame-cibo-killy-monochrome"}]},
		    iextract: {"input":{"link":"https://konachan.com/post/show/72141/blame-cibo-killy-monochrome"}}
		}
	]
};

testCases["mangadex"] =
{
	"cases":
	[
		{
			mextract: {"output":{"name":"Blame!", "link":"https://mangadex.org/title/2712/blame"}},
			cextract: {"input":{"link":"https://mangadex.org/title/2712/blame"},
					   "chapters":[{version: "gb - Omanga", volnumb: 1, chanumb: "1", link: "https://mangadex.org/api/?id=13563&server=null&type=chapter"}]},
			pextract: {"input":{"link":"https://mangadex.org/api/?id=13563&server=null&type=chapter"},
					   "pages":[{src: "https://s2.mangadex.org/data//45e505785390f08527ab47207b5d270c/1.jpg", numb: "0001"}]}
		}
	]
};

testCases["mangafox"] =
{
	"cases":
	[
		{
			mextract: {"output":{"name": "Blame!", "link": "https://fanfox.net/manga/blame/"}},
			cextract: {"input":{"link":"https://fanfox.net/manga/blame"},
					   "chapters":[{volnumb: "01", chanumb: "1", link: "https://m.fanfox.net/manga/blame/v01/c001/1.html"}]},
			pextract: {"input":{"link":"https://m.fanfox.net/manga/blame/v01/c001/1.html"},
					   "pages":[{"numb":"0001","link":"https://m.fanfox.net/manga/blame/v01/c001/1.html"}]},
		    iextract: {"input":{"link":"https://m.fanfox.net/manga/blame/v01/c001/1.html"}}
		}
	]
};

testCases["mangahere"] =
{
	"cases":
	[
		{
			mextract: {"output":{"name":"Blame!", "link":"https://www.mangahere.cc/manga/blame/"}},
			cextract: {"input":{"link":"https://www.mangahere.cc/manga/blame/"},
					   "chapters":[{volnumb: null, chanumb: "001", link: "https://www.mangahere.cc/manga/blame/c001/1.html"}]},
			pextract: {"input":{"link":"https://www.mangahere.cc/manga/blame/c001/"},
					   "pages":[{"numb":"0001","link":"https://m.mangahere.cc/manga/blame/c001/1.html"}]},
		    iextract: {"input":{"link":"https://m.mangahere.cc/manga/blame/c001/1.html"}}
		}
	]
};

testCases["mangahub"] =
{
	"cases":
	[
		{
			mextract: {"output":{"name":"Blame!", "link":"https://mangahub.io/manga/blame"}},
			cextract: {"input":{"link":"https://mangahub.io/manga/blame"},
					   "chapters":[{volnumb: undefined, chanumb: "1", link: "https://mangahub.io/chapter/blame/chapter-1"}]},
			pextract: {"input":{"link":"https://mangahub.io/chapter/blame/chapter-1"},
					   "pages":[{"numb":"0001","src":"https://cdn.mangahub.io/file/imghub/blame/1/1.jpg"}]}
		}
	]
};

testCases["manganel"] =
{
	"cases":
	[
		{
			mextract: {"output":{"name":"Blame!", "link":"https://manganelo.com/manga/blame"}},
			cextract: {"input":{"link":"https://manganelo.com/manga/blame"},
					   "chapters":[{volnumb: "1", chanumb: "1", link: "https://manganelo.com/chapter/blame/chapter_1"}]},
			pextract: {"input":{"link":"https://manganelo.com/chapter/blame/chapter_1"},
					   "pages":[{"numb":"0001","src":"https://s3.mkklcdnv3.com/mangakakalot/b1/blame/vol1_chapter_1_the_nets_offspring/1.jpg"}]}
		}
	]
};

testCases["mangapark"] =
{
	"cases":
	[
		{
			mextract: {"output":{"name": "Blame!", "link": "https://mangapark.org/series/2984/blame"}},
			cextract: {"input":{"link":"https://mangapark.org/series/2984/blame"},
					   "chapters":[{volnumb: null, chanumb: "1", version: "[English] BOT-FISH", link: "https://mangapark.org/chapter/77422/blame-ch-1"}]},
			pextract: {"input":{"link":"https://mangapark.org/chapter/77422/blame-ch-1"},
					   "pages":[{numb: "0001", src: "https://z-img-03.mangapark.org/images/15/7d/157d730a1fa18f711641a7b87a20bf91ff602ebb_20419_641_1058.jpg"}]}
		}
	]
};

testCases["mangaseeonline"] =
{
	"cases":
	[
		{
			mextract: {"output":{"name":"Blame!", "link":"https://mangaseeonline.us/manga/Blame"}},
			cextract: {"input":{"link":"https://mangaseeonline.us/manga/Blame"},
					   "chapters":[{volnumb: 1, chanumb: "1", link: "https://mangaseeonline.us/read-online/Blame-chapter-1-page-1.html"}]},
			pextract: {"input":{"link":"https://mangaseeonline.us/read-online/Blame-chapter-1-page-1.html"},
					   "pages":[{"numb":"0001","link":"https://mangaseeonline.us/read-online/Blame-chapter-1-page-1.html"}]},
			iextract: {"input":{"link":"https://mangaseeonline.us/read-online/Blame-chapter-1-page-1.html"}}
		}
	]
};

testCases["mangatown"] =
{
	"cases":
	[
		{
			mextract: {"output":{"name": "Blame!", "link": "https://www.mangatown.com/manga/blame/"}},
			cextract: {"input":{"link":"https://www.mangatown.com/manga/blame"},
					   "chapters":[{volnumb: null, chanumb: "001", link: "https://www.mangatown.com/manga/blame/c001/"}]},
			pextract: {"input":{"link":"https://www.mangatown.com/manga/blame/c001/"},
					   "pages":[{"numb":"0001","link":"https://www.mangatown.com/manga/blame/c001/"}]},
			iextract: {"input":{"link":"https://www.mangatown.com/manga/blame/c001/"}}
		}
	]
};

testCases["readcomicsonline"] =
{
	"cases":
	[
		{
			mextract: {"output":{"name":"1985: Black Hole Repo (2017)", "link":"https://readcomicsonline.ru/comic/1985-black-hole-repo-2017"}},
			cextract: {"input":{"link":"https://readcomicsonline.ru/comic/1985-black-hole-repo-2017"},
					   "chapters":[{volnumb: 1, chanumb: "1", link: "https://readcomicsonline.ru/comic/1985-black-hole-repo-2017/1"}]},
			pextract: {"input":{"link":"https://readcomicsonline.ru/comic/1985-black-hole-repo-2017/1"},
					   "pages":[{numb: "0001", link: "https://readcomicsonline.ru/comic/1985-black-hole-repo-2017/1/1"}]},
			iextract: {"input":{"link":"https://readcomicsonline.ru/comic/1985-black-hole-repo-2017/1/1"}}
		}
	]
};

testCases["readmanga"] =
{
	"cases":
	[
		{
			mextract: {"output":{"name":"Blame!", "link":"https://www.readmng.com/blame"}},
			cextract: {"input":{"link":"https://www.readmng.com/blame"},
					   "chapters":[{volnumb: 1, chanumb: "1", link: "https://www.readmng.com/blame/1"}]},
			pextract: {"input":{"link":"https://www.readmng.com/blame/1"},
					   "pages":[{numb: "0001", src: "https://cdn.readmng.com/uploads/chapter_files/900/0/p_00001.jpg"}]}
		}
	]
};

testCases["webtoons.com"] =
{
	"cases":
	[
		{
			mextract: {"output":{"name":"Visitor", "link":"https://www.webtoons.com/en/challenge/visitor/list?title_no=150722"}},
			cextract: {"input":{"link":"https://www.webtoons.com/en/challenge/visitor/list?title_no=150722"},
					   "chapters":[{volnumb: null, chanumb: null, link: "https://www.webtoons.com/en/challenge/visitor/chapter-1/viewer?title_no=150722&episode_no=1"}]},
			pextract: {"input":{"link":"https://www.webtoons.com/en/challenge/visitor/chapter-1/viewer?title_no=150722&episode_no=1"},
					   "pages":[{numb: "0001", src: "https://webtoon-phinf.pstatic.net/20180208_284/1518071957528GufDu_JPEG/96fe4cf0-a941-4ec7-bf1f-f4bf8c8b8807.jpg?type=q90"}]}
		}
	]
};

module.exports = testCases;