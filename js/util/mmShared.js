"use strict"; 

angular.module('app').factory('mmShared', ['ngUtil',
function(ngUtil)
{
	let factory = {};

	let logDebug = (enable, obj) => {if(enable) console.log("debug", obj)};

	factory.mProcessResponse = function(body, options, i)
	{
		let dom = new DOMParser().parseFromString(body, "text/html");
		
		let selectElements = options.selectFct(dom, i) || [];
		if(typeof selectElements == "string")
			return selectElements;

		let mlistPart = [];
		for(let selectElement of selectElements)
		{
			let mlistElements = options.elementFct ? options.elementFct(selectElement, i, dom) : selectElement;
			if(typeof mlistElements == "string")
				return mlistElements;

			if(!Array.isArray(mlistElements))
				mlistElements = [mlistElements];
			mlistElements = mlistElements.filter(Boolean);

			for(let mlistElement of mlistElements)
			{
				if(mlistElement.link)
				{	
					mlistElement.name = mlistElement.name.trim();
					mlistPart.push(mlistElement);
				}
				else 
					logDebug(options.debug, {"method":"commonMextract", "mlistElement":mlistElement, "index":i, "warning":"received mlist element without link"});
			}
		}

		logDebug(options.debug, {"method":"commonMextract", "index":i, "mlistPart":mlistPart});
		return mlistPart;
	}
	factory.mGetResponse = async function(options, i)
	{
		let requestCfg = JSON.parse(JSON.stringify(options.requestCfg));
		if(typeof options.requestCfg.url == "function")
			requestCfg.url = options.requestCfg.url(i);

		let requestWrapper = require('./requestWrapper.js');
		let response = await requestWrapper.getRequest(options, requestCfg);
		logDebug(options.debug, {"method":"commonMextract", "index":i, "serverResponse":response});
		return response;
	}
	factory.commonMextract = async function(options)
	{
		if(ngUtil.currentmmodel.options.debug)
			options.debug = true;

		options.options = options.options || {};
		options.options.maxIterations = options.options.maxIterations || 1;
		
		logDebug(options.debug, {"method":"commonMextract", "input":options});

		if(options.options.setMaxIterations)
		{
			let response = await factory.mGetResponse(options, 0);
			let dom = new DOMParser().parseFromString(response.data, "text/html");
			options.options.maxIterations = parseInt(options.options.setMaxIterations(dom));
			logDebug(options.options.debug, {"method":"commonMextract", "maxIterations":options.options.maxIterations});
		}

		let mmodel = ngUtil.currentmmodel;
		let mlist = [];
		for(let i=0; i<options.options.maxIterations; i++)
		{
			try
			{
				let response = await factory.mGetResponse(options, i);	
				if(mmodel.cancelMlist)
				{
					mmodel.cancelMlist = false;
					return;
				}
	
				let mlistPart = factory.mProcessResponse(response.data, options, i)
				if((options.options.cancelOnEmpty && !mlistPart) || mlistPart == "cancel")
					break;
				if(mlistPart == "abort")
					return;
	
				if(mlistPart)
					mlist = mlist.concat(mlistPart);

				ngUtil.mlistOnProgress && ngUtil.mlistOnProgress( 100*(i/options.options.maxIterations) );

				if(options.options.latency)
					await new Promise( (resolve) => setTimeout(resolve, options.options.latency) );
			}
			catch(e)
			{
				console.error(e);
			}
		}

		if(options.options.dedupe)
			mlist = mlist.filter((thing, index, self) => self.findIndex(t => t.link === thing.link) === index);

		logDebug(options.options.debug, {"method":"commonMextract", "mlist":mlist});
		return mlist;
	}

	factory.applyChaNumbRegex = (text) =>
	{
		let charegex = /(ch?a?\.|chapt?e?r?)[^\d]{0,3}(\d+(\.?\d*)*)/gi.exec(text);
		if(charegex)
			return charegex[2];
		charegex = /(cha?p?t?e?r?\:?\ ?|ch\:\ ?)(\d+(\.?\d*)*)/gi.exec(text);
		if(charegex)
			return charegex[2];
		return null;
	}
	factory.applyVolNumbRegex = (text) =>
	{
		let volregex =  /(vo?l?\.|volume)[^\d]{0,3}(\d+)/gi.exec(text);
		if(volregex)
			return volregex[2];
		volregex = /(vol?u?m?e?\:?\ ?|v\:\ ?)(\d+(\.?\d*)*)/gi.exec(text);
		if(volregex)
			return volregex[2];
		return null;
	}
	factory.normalizeChaNumb = (chanumb, previousChapterElement) =>
	{
		if(chanumb == null || chanumb === "")
		{
			if(previousChapterElement)
				chanumb = isNaN(parseFloat(previousChapterElement.chanumb)) ? previousChapterElement.chanumb : parseFloat(previousChapterElement.chanumb) + 1;
			else
				chanumb = 1;
		}
		if(isNaN(parseFloat(chanumb)))
			return chanumb;

		let normchanumb = parseInt(chanumb).toString().padStart(4, '0');
		chanumb = chanumb.toString();
		if(chanumb.includes("."))
		{
			let chanumbFractional = chanumb.substring(chanumb.indexOf(".") + 1, chanumb.length);
			normchanumb = (isNaN(parseInt(chanumbFractional))) ? normchanumb + "." + chanumbFractional : normchanumb + "." + parseInt(chanumbFractional);
		}
		return normchanumb;	
	}
	factory.normalizeVolNumb = (volnumb, previousChapterElement) =>
	{
		if(volnumb == null || volnumb === "")
			volnumb = previousChapterElement ? previousChapterElement.volnumb : 1;
		return isNaN(parseInt(volnumb)) ? volnumb : parseInt(volnumb).toString().padStart(4, '0');
	}
	factory.normalizeChapterNumbers = (chapters) =>
	{
		return chapters.map( (chapterElement, i) => 
		{
			let previousChapterElement;
			if(isNaN(chapterElement.volnumb))
				chapterElement.volnumb = null;
			if(isNaN(chapterElement.chanumb))
				chapterElement.chanumb = null;
			if(chapterElement.volnumb == null || chapterElement.chanumb == null)
			{
				for(let k=i-1; k>=0; k--)
				{
					if(chapters[k].version == chapterElement.version)
					{
						previousChapterElement = chapters[k];
						break;
					}
				}
			}

			chapterElement.volnumb = factory.normalizeVolNumb(chapterElement.volnumb, previousChapterElement);
			chapterElement.chanumb = factory.normalizeChaNumb(chapterElement.chanumb, previousChapterElement);
			return chapterElement;
		});
	}
	factory.cProcessResponse = async function(body, options)
	{
		let chapters = [];
		let dom = new DOMParser().parseFromString(body, "text/html");
		let selectElements = await options.selectFct(dom) || [];
		
		for(let selectElement of selectElements)
		{	
			let chapterElements = options.elementFct ? await options.elementFct(selectElement, dom) : selectElement;
			if(!Array.isArray(chapterElements))
				chapterElements = [chapterElements];
			chapters = chapters.concat(chapterElements.filter(Boolean));
		}
		
		logDebug(options.debug, {"method":"commonCextract","chapters":chapters});
		return chapters;
		
	}
	factory.commonCextract = async function(options)
	{
		try
		{
			if(ngUtil.currentmmodel.options.debug)
				options.debug = true;
			logDebug(options.debug, {"method":"commonCextract", "input":options});
			let requestWrapper = require('./requestWrapper.js');
			let response = await requestWrapper.getRequest(options, options.requestCfg);
			logDebug(options.debug, {"method":"commonCextract", "serverResponse":response});
			let chapters = await factory.cProcessResponse(response.data, options);

			if(chapters.length==0)
				throw new Error("chapters array empty");
			
			return chapters;
		}
		catch(e)
		{
			throw e;
		}
	}
	
	return factory;
}]);