"use strict";

angular.module('app').factory('testFactory', ['mmShared',
function (mmShared)
{
	let factory = {};	
	debugLog(factory);
	
	let urlpattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
								'((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name and extension
								'((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
								'(\\:\\d+)?'+ // port
								'(\\/[-a-z\\d%@_.~+&:]*)*'+ // path
								'(\\?[;&a-z\\d%@_.,~+&:=-]*)?'+ // query string
								'(\\#[-a-z\\d_]*)?$','i'); // fragment locator
	factory.mmodelsTestWrapper = async (type, testCases, testMmodel, logresponse = true, mmodelWorker) =>
	{
		try
		{
			console.info(type + ", mmodel " + testMmodel.name + ": test starting");
			await factory.mmodelsTestMethods[type](testCases, testMmodel, logresponse, mmodelWorker);
			console.info(type + ", mmodel " + testMmodel.name + ": test finished");	
		}
		catch(e)
		{
			console.error(e);
		}
	}
	factory.mmodelsTestMethods = {};
	factory.mmodelsTestMethods.mextract = async (testCases, testMmodel, logresponse = true) =>
	{
		let mlist = await testMmodel.mextract(mmShared);
		if(logresponse)
			console.log(mlist);
		
		console.assert(mlist.length>200, "mextract, mmodel " + testMmodel.name + ": mlist length = " + mlist.length);
		for(let testCase of testCases)
		{
			let foundElement = mlist.find( elem => elem.name==testCase.mextract.output.name && elem.link==testCase.mextract.output.link );
			console.assert(foundElement, "mextract, mmodel " + testMmodel.name + ": case", testCase.mextract, "not found in", mlist);
		}
	}	

	factory.mmodelsTestMethods.cextract = async (testCase, testMmodel, logresponse = true) =>
	{
		let chapters = await testMmodel.cextract(mmShared, testCase.cextract.input);
		if(logresponse)
			console.log(chapters);

		console.assert(chapters.length > 0, "cextract, mmodel " + testMmodel.name + ": empty chapter array ");
		for(let caseChapter of testCase.cextract.chapters)
		{
			let foundChapter = chapters.some( chapter =>
				caseChapter.volnumb == chapter.volnumb && caseChapter.chanumb == chapter.chanumb && caseChapter.link == chapter.link);
			console.assert(foundChapter, "cextract, mmodel " + testMmodel.name + ", case", caseChapter, "not found in", chapters);
		}
	}

	factory.mmodelsTestMethods.pextract = async (testCase, testMmodel, logresponse = true, mmodelWorker) =>
	{
		let response = await nodeUtil.getThreadResult(mmodelWorker, {"method":"pextract", "curchap": testCase.pextract.input });

		if(logresponse)
			console.log(response);
		if(!response.data)
			response.data = [];
		
		console.assert(response.data.length > 0, "pextract, mmodel " + testMmodel.name + ": empty page array ");

		for(let casePage of testCase.pextract.pages)
		{
			let foundPage = response.data.some( page =>
				casePage.numb == page.numb && casePage.link == page.link && casePage.src == page.src);
			console.assert(foundPage, "pextract, mmodel " + testMmodel.name + ", case", casePage, "not found in", response.data);
		}
	}								

	factory.mmodelsTestMethods.iextract = async (testCase, testMmodel, logresponse = true, mmodelWorker) =>
	{
		let response = await nodeUtil.getThreadResult(mmodelWorker, {"method":"iextract", "curpage": testCase.iextract.input});
		if(logresponse)
			console.log(response);
	
		response.data = response.data || {};
		console.assert(response.data.src && response.data.src.length>0,
				 "iextract, mmodel " + testMmodel.name + ": received empty src " + response.data.src);
		if(response.data.src)
			console.assert(urlpattern.test(response.data.src.replace(/(\ )/g,'')),
				 "iextract, mmodel " + testMmodel.name + ": received src " + response.data.src + " is not a valid url");
	}	

	factory.mmodelsTest = async function(mmodelname = "all", type = "all", logresponse = true)
	{
		console.info("starting tests");

		let testCases = factory.testCases = require(userPaths.jsBase + "testCases.js");
		for(let testMmodel of factory.mmodels)
		{
			if(!testCases[testMmodel.name])
			{
				console.warn("no cases found for " + testMmodel.name);
				continue;
			}			
			if(mmodelname!="all" && mmodelname!=testMmodel.name)
				continue;

			let mmodelWorker = new Worker(userPaths.mmodelWorker);
			await nodeUtil.getThreadResult(mmodelWorker, {"method":"setOptions", "options": testMmodel.options,
				"paths":{ "jsBase":userPaths.jsBase, "mmodel":testMmodel.path } });

			for(let testCase of testCases[testMmodel.name].cases)
			{
				if(type=="cextract" || type=="all")
					await factory.mmodelsTestWrapper("cextract", testCase, testMmodel, logresponse, mmodelWorker);
				await new Promise( (resolve) => setTimeout(resolve, testMmodel.latency) );																					
				if(type=="pextract" || type=="all")
					await factory.mmodelsTestWrapper("pextract", testCase, testMmodel, logresponse, mmodelWorker);
				await new Promise( (resolve) => setTimeout(resolve, testMmodel.latency) );			
				if((type=="iextract" || type=="all") && testMmodel.iextract)
					await factory.mmodelsTestWrapper("iextract", testCase, testMmodel, logresponse, mmodelWorker);
			}
		}

		for(let testMmodel of factory.mmodels)
		{	
			if((type=="mextract" || type=="all") && testCases[testMmodel.name] && testMmodel.mextract)
				await factory.mmodelsTestWrapper("mextract", factory.testCases[testMmodel.name].cases, testMmodel, logresponse);
		}

		console.info("all tests finished");
	}	
	return factory;
}]);