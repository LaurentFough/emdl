"use strict";

module.exports.getRequest = async (options = {}, requestCfg) =>
{
	let response;
	if(!options.options)
		options.options = {};


	if(options.options.requestLibrary == "hooman")
	{
		const hooman = require('hooman');
		// const hooman = await import(/* webpackChunkName: "hooman" */ 'hooman');
		if(requestCfg.responseType == "stream")
		{
			response = {};
			let gotResponse = await hooman.stream(Object.assign({ timeout: 60000 }, requestCfg));
			response.data = gotResponse;
		}
		else
		{
			response = await hooman(Object.assign({ timeout: 60000 }, requestCfg));
			response.data = response.body;
		}
	}
	if(!response || options.options.requestLibrary == "axios")
	{
        const axios = require('axios');
        axios.defaults.adapter = require('axios/lib/adapters/http');
		response = await axios(Object.assign({timeout: 60000}, requestCfg));	
	}
	return response;
}
