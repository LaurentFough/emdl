"use strict";

angular.module('app').directive('aceEditor', function() {
	return{
		require: 'ngModel',
		link: function(scope, elem, attrs, ctrl)
		{
			let aceEditorInstance = ace.edit(elem[0]);
			aceEditorInstance.setShowPrintMargin(false);
			aceEditorInstance.setTheme("ace/theme/twilight");
			aceEditorInstance.session.setMode("ace/mode/json");
			aceEditorInstance.setOption("maxLines", 25);
			let cancelSetValue;
			aceEditorInstance.session.on('change', function(delta)
			{
				if(cancelSetValue)
					return;

				let jsonData;
				try{ jsonData = angular.fromJson(aceEditorInstance.getValue()); } catch(error){ return; }
				for(let key in ctrl.$modelValue)
					delete ctrl.$modelValue[key];
				scope.$apply(() => {
					for(let key in jsonData)
						ctrl.$modelValue[key] = jsonData[key];
				});
			});
			ctrl.$formatters.push((obj) => {
				let editorData = obj == "" || obj == undefined ? "" : angular.toJson(obj, 4);
				cancelSetValue = true;
				aceEditorInstance.setValue(editorData, -1);
				cancelSetValue = false;
			});
		}
	}
});

angular.module('app').directive('staticInclude',
['$compile',
( $compile) =>
{
	return {
		restrict : 'A',
        scope: {resumeWatchers:"="},
		link: async (scope, element, attrs) =>
		{
			let templatePath = attrs.staticInclude;
            scope.parent = scope.$parent;
            let response = await fetch(templatePath);
            response = await response.text();
			$compile(element.html(response).contents())(scope);
			if(attrs.$attr.resumeWatchers)
			{
				scope.$parent.$watch(() => scope.resumeWatchers, () =>
				{
					if(scope.resumeWatchers === undefined)
						setTimeout(scope.$suspend);
					else
						scope.resumeWatchers ? scope.$resume() : scope.$suspend()
				});
			}
		}
	}
}]);

angular.module('app').factory('ngUtil',
['$rootScope', '$mdDialog', '$mdSelect', 'themeProvider', '$mdTheming',
( $rootScope ,  $mdDialog ,  $mdSelect ,  themeProvider ,  $mdTheming) =>
{
	let factory = {};

	factory.addMissingProps = (reference, config, excludeProps = []) =>
	{
		for(let prop in reference)
		{
			if(!config.hasOwnProperty(prop))
				config[prop] = reference[prop];
			if(typeof reference[prop] == "object" && !excludeProps.includes(prop))
				factory.addMissingProps(reference[prop], config[prop], excludeProps);
		}
	}
	//color theme config	
	$rootScope.theme = "defaultNo0";
	factory.colorPalettes = Object.keys($mdTheming.PALETTES).sort();
	factory.themeProvider = themeProvider;
	factory.updateColorTheme = () =>
	{
		$rootScope.theme = "defaultNo" + (parseInt($rootScope.theme.split("No")[1])+1);
		let themeConfig = $rootScope.$storage.config.colorTheme;
		if(!factory.colorPalettes.includes(themeConfig.primaryPalette))
			themeConfig.primaryPalette = "light-blue";
		if(!factory.colorPalettes.includes(themeConfig.accentPalette))
			themeConfig.accentPalette = "red";
		if(!factory.colorPalettes.includes(themeConfig.warnPalette))
			themeConfig.warnPalette = "yellow";
		if(!factory.colorPalettes.includes(themeConfig.backgroundPalette))
			themeConfig.backgroundPalette = "grey";

		themeProvider.theme($rootScope.theme)
					 .primaryPalette(themeConfig.primaryPalette)
					 .accentPalette(themeConfig.accentPalette)
					 .warnPalette(themeConfig.warnPalette)
					 .backgroundPalette(themeConfig.backgroundPalette)
					 .dark(themeConfig.darkTheme);
		$mdTheming.generateTheme($rootScope.theme);
		themeProvider.setDefaultTheme($rootScope.theme);

	}
	factory.closeMdSelect = () =>
	{
		// let mdSelectElement = document.querySelector('body > .md-select-3-columns');
        // console.log("TCL: mdSelectElement", mdSelectElement)
		// mdSelectElement && mdSelectElement.remove();
		$mdSelect.hide();
	}

	factory.setBackgroundThrottling = () => electronWindow.webContents.setBackgroundThrottling($rootScope.$storage.config.enableBackgroundThrottling);
	factory.throttle = (fn, delay) => {
		let lastCall = 0;
		return function (...args) {
		  const now = (new Date).getTime();
		  if (now - lastCall < delay) {
			return;
		  }
		  lastCall = now;
		  return fn(...args);
		}
	}
	factory.debounce = (fn, wait = 500) =>
	{
	  let timeout
	  return function (...args) {
		clearTimeout(timeout)
		timeout = setTimeout(() => fn.call(this, ...args), wait)
	  }
	}

	factory.openWebConsole = electronWindow.openDevTools;
	factory.sortByAttr = (attr) => (a, b) =>
	{
		if(a[attr].toLowerCase() > b[attr].toLowerCase())
			return 1;
		else
			return -1;
	};
	
	factory.showDialog = (selector, ev, onComplete, onRemoving) =>
	{
		return $mdDialog.show(
		{
			contentElement: selector,
			parent: angular.element(document.body),
			targetEvent: ev,
			clickOutsideToClose: true,
			onComplete: onComplete,
			onRemoving : onRemoving
		});
	};
	factory.openExternal = electron.shell.openExternal;

	factory.textInputMenu = [];
	for(let item of ["Cut","Copy","Paste","Delete"])
		factory.textInputMenu.push(
		[item, function ($itemScope, $event, modelValue, text, $li)
		{
			$event.target.focus();
			electronWindow.webContents[item.toLowerCase()]();
		}]);
	factory.openInExplorerMenu = 
	[
		['Open External', function ($itemScope, $event, modelValue, text, $li) {
			electron.shell.openExternal(modelValue.replace(/\\/g, "/"));
		}],
		null
	].concat(factory.textInputMenu);
	
	return factory;
}]);