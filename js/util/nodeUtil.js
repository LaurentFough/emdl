"use strict";

const fs = require('fs-extra');
const nodeUtil = {};

nodeUtil.getFiles = async (dir) =>
{
	let files = await fs.readdir(dir);
	return files.map( name => { return { name:name, path: dir + '/' + name }; });
}
nodeUtil.getFilesRecursive = async (dir) => {
	const subdirs = await fs.readdir(dir);
	const files = await Promise.all(subdirs.map(async (subdir) => {
	  const res = dir +"/"+subdir;
	  return (await fs.stat(res)).isDirectory() ? nodeUtil.getFilesRecursive(res) : res;
	}));
	return Array.prototype.concat(...files);
}

const collator = new Intl.Collator(undefined, {numeric: true, sensitivity: 'base'});
nodeUtil.sortFilesHelper = function(a, b, numericSorting=true)
{
	let aarr = a.toLowerCase().split("/");
	let barr = b.toLowerCase().split("/");
	// aarr[aarr.length - 1] = aarr[aarr.length - 1].split(".")[0];
	// barr[barr.length - 1] = barr[barr.length - 1].split(".")[0];

	if(aarr.length<barr.length)
		return -1;
	if(aarr.length>barr.length)
		return 1;

	for(let i=0;i<aarr.length;i++)
	{
		if(numericSorting)
		{
			let compvalue = collator.compare(aarr[i], barr[i]);
			if(compvalue)
				return compvalue;
		}
		else
		{
			if(aarr[i]<barr[i])
				return -1;
			if(aarr[i]>barr[i])
				return 1;
		}
	}

	// return 0; for different file paths crashes sort ( collator.compare("01.jpg", "1.jpg") == 0)
	if(a.toLowerCase()<b.toLowerCase()) 
		return -1;
	else
		return 1;
};

nodeUtil.sortFiles = function(numericSorting){
	return function(a, b)
	{
		let sortVal = nodeUtil.sortFilesHelper(a.path, b.path, numericSorting);
		if(sortVal)
			return sortVal;
		return nodeUtil.sortFilesHelper(a.zipPath, b.zipPath, numericSorting);
	}
};

nodeUtil.sortBaseNames = function(a, b)
{
	let aExt = path.extname(a.name);
	let bExt = path.extname(b.name);
	aExt = aExt==".zip" ? "" : aExt;
	bExt = bExt==".zip" ? "" : bExt;
	if(aExt && !bExt)
		return 1;
	if(!aExt && bExt)
		return -1;
	return nodeUtil.sortFilesHelper(a.name, b.name, true)
}

//wrapper for child process execution
nodeUtil.getThreadResult = function(worker,input)
{
	// console.log(input);
	return new Promise(function(resolve,reject)
	{
		worker.promiseQueue = worker.promiseQueue || [];
		worker.promiseQueue.push( {resolve:resolve, reject:reject} );
		worker.onmessage = function (ev) {
			// console.log(ev.data);
			if(ev.data.hasOwnProperty("debug"))
				Array.isArray(ev.data.debug) ? console.log("debug", ...ev.data.debug) : console.log("debug", ev.data.debug);
			else
				if(ev.data.hasOwnProperty("error"))
					worker.promiseQueue.pop().reject(ev.data.error);
				else
					worker.promiseQueue.pop().resolve(ev.data);
		};
		worker.onerror = function (ev) {
			worker.promiseQueue.pop().reject(ev);
		};
		worker.postMessage(input);
	});
}

nodeUtil.removeFileExtension = (path) => path ? path.substring(0,path.lastIndexOf(".")) : path;
nodeUtil.hexToRgb = (hex) =>
{
	hex   = hex.replace('#', '');
	let r = parseInt(hex.length == 3 ? hex.slice(0, 1).repeat(2) : hex.slice(0, 2), 16);
	let g = parseInt(hex.length == 3 ? hex.slice(1, 2).repeat(2) : hex.slice(2, 4), 16);
	let b = parseInt(hex.length == 3 ? hex.slice(2, 3).repeat(2) : hex.slice(4, 6), 16);
	return [r,g,b];
}

nodeUtil.getHashWatermarkConfig = (hashConfigs) => 
{
	const ImagePHash = require("../../lib/phash.js");
	let watermarkConfig = [];
	for(let hashConfig of hashConfigs)
	{
		let watermarkConfigObj = {};
		watermarkConfigObj.matchFunction = (image) => 
		{
			let imageHash = image.clone().crop(hashConfig.area.x, hashConfig.area.y, hashConfig.area.w, hashConfig.area.h).hash();
			if( hashConfig.hash.some( hash => new ImagePHash().distance(imageHash, hash)<0.1 ) )
				return true;
			return false;
		};
		watermarkConfigObj.removeFunction = (image) => image.crop(0, 0, image.bitmap.width, image.bitmap.height-(hashConfig.area.h+5));
		watermarkConfig.push(watermarkConfigObj);
	}
	return watermarkConfig;
}

module.exports = nodeUtil;