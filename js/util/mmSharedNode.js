'use strict';

let cheerio  = require('cheerio');

let debugLog = (enable, method, msg) => enable && console.log(Object.assign(msg, {"method": method}) );
let mmSharedNode = {};

mmSharedNode.normalizePageNumbers  = (pages) =>
{
	return pages.map( (page, i) => 
	{
		if(page.numb == null)
			page.numb = i+1;
		page.numb = isNaN(parseFloat(page.numb)) ? page.numb : page.numb.toString().padStart(4, '0');
		return page;
	});
}

mmSharedNode.commonPextract = async function(options)
{
	if(mmSharedNode.debug)
		options.debug = true;
	debugLog(options.debug, "commonPextract", {"input": options});
	let requestWrapper = require('./requestWrapper.js');
	let response = await requestWrapper.getRequest(options, options.requestCfg);
	debugLog(options.debug, "commonPextract", {"serverResponse": response});

	let pages = [];
	let $ = cheerio.load(response.data);
	
	if(options.elementFct)
		await options.selectFct($).each(async function(i) { pages.push(await options.elementFct($(this),i)); });
	else
		pages = await options.selectFct($);

	pages = pages.filter(Boolean);
	debugLog(options.debug, "commonPextract", {"pages": pages});
	return pages;
}

mmSharedNode.commonIextract = async function(options)
{
	if(mmSharedNode.debug)
		options.debug = true;
	debugLog(options.debug, "commonIextract", {"input": options});
	let requestWrapper = require('./requestWrapper.js');
	let response = await requestWrapper.getRequest(options, options.requestCfg);
	debugLog(options.debug, "commonIextract", {"serverResponse": response});
	let result = options.elementFct(cheerio.load(response.data));
	debugLog(options.debug, "commonIextract", {"imageSource": result});
	return result;	
}

module.exports = mmSharedNode;