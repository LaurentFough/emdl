const {app, BrowserWindow, ipcMain, nativeImage} = require('electron');

if(!app.requestSingleInstanceLock())
	app.quit();
else
{
	let createWindow = () =>
	{
		const ElectronWindowState = require('./lib/electronWindowStateManager.js');
		const windowState = new ElectronWindowState({ defaultState: {width: 1100, height: 600} });
		let mainWindow = new BrowserWindow(
		{
			'name': "emdl",
			'x': windowState.state.x,
			'y': windowState.state.y,
			'width': 0,
			'height': 0,
			'autoHideMenuBar': true,
			'backgroundColor': '#000000',
			'webPreferences': {
				'enableRemoteModule': true,
				'nodeIntegration': true,
				'nodeIntegrationInWorker': true,
				'spellcheck': false,
				'webviewTag': true
			}
		});
		mainWindow.removeMenu();
		mainWindow.setSize(windowState.state.width, windowState.state.height);
		windowState.bindWindow(mainWindow);
		mainWindow.loadURL('file://' + __dirname + '/index.html');
	};	
	process.chdir(__dirname.replace("app.asar","app.asar.unpacked"));

	app.commandLine.appendSwitch('autoplay-policy', 'no-user-gesture-required');
	app.on('window-all-closed', () => app.quit());

	ipcMain.on('ondragstart', (event, filePath, iconData) => 
	{
		event.sender.startDrag(
		{
			file: filePath,
			icon: nativeImage.createFromDataURL(iconData)
		});
	});

	app.whenReady().then(createWindow);
}