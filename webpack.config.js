const fs = require("fs-extra");
const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');
const webpack = require('webpack');

const buildPath = path.resolve(__dirname, "./build");
fs.removeSync(buildPath)

const frontEndConfig =
{
  target: "web",
  externals: [],
  resolve: {
    modules: [__dirname, "node_modules"]
  },
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: './fonts/'
            }
          }
        ]
      }
    ],
  },
  entry: {
    app: ["./js/frontEnd.entry.js"]
  },
  output: {
    path: buildPath,
    publicPath: "./build/",
    filename: "frontEnd.bundle.js"
  },
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin()],
  }
}

const appConfig =
{
  target: "node",
  externals: [],
  resolve: {
    modules: [__dirname, "node_modules"]
  },
  plugins: [
    new webpack.DefinePlugin({ "global.GENTLY": false })
  ],
  entry: {
    app: ["./js/app.entry.js"]
  },
  output: {
    path: buildPath,
    publicPath: "./build/",
    filename: "app.bundle.js",
    chunkFilename: '[name].bundle.js'
  },
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin()],
  },
  plugins: [
    new webpack.IgnorePlugin(/canvas$/)
  ]
}

module.exports = [ frontEndConfig, appConfig ];

